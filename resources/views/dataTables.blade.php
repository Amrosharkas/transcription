<?php $i=3; $j =2;?>
@extends('admin.master')
@section('add_css')
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
   
@stop

@section('add_js_plugins')
	<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/table-managed.js"></script>
    <script>
		
		$(document).ready(function() {
			var table = $('#users-table').DataTable({
				
			processing: true,
			serverSide: true,
			ajax: {
				url :'/multi-filter-select-data',
				data: function (d) {
                d.name = $('input[name=name]').val();
                d.email = $('input[name=email]').val();
				d.date_start = $('input[name=date_start]').val();
				d.date_end = $('input[name=date_end]').val();
				d.date_from = $('input[name=date_from]').val();
				d.date_to = $('input[name=date_to]').val();
				d.score_from = $('input[name=score_from]').val();
				d.score_to = $('input[name=score_to]').val();
				d.checked = $('#checked').val();
            }
			},
			columns: [
				{data: 'Checkbox', name: 'Checkbox', orderable: false, searchable: false},
				{data: 'name', name: 'name'},
				{data: 'test_score', name: 'test_score'},
				{data: 'start_time', name: 'start_time'},
				{data: 'end_time', name: 'end_time'},
				{data: 'duration', name: 'duration', searchable: false},
				{data: 'Test', name: 'Test', orderable: false, searchable: false},
				{data: 'Checked', name: 'Checked', orderable: false, searchable: false}
			],
			
			initComplete: function () {
				var checkboxes = $("input[type='checkbox']"),
				submitButt = $(".multiple-controls");
				checkboxes.click(function() {
				submitButt.attr("disabled", !checkboxes.is(":checked"));
			});
			}
		});
		
		$("#search-form").submit(function(e){
       	 e.preventDefault();
		 table.draw();
    	});
		$('a.toggle-vis').on( 'click', function (e) {
			e.preventDefault();
	 		$(this).toggleClass('bold');
			// Get the column API object
			var column = table.column( $(this).attr('data-column') );
	 
			// Toggle the visibility
			column.visible( ! column.visible() );
		} );
			
			
		});
</script>

<script type="text/javascript">


                $(document).on('change','.update-checked',function (){
                    postData = {
                        '_token': '{{csrf_token()}}',
                        'user_id': $(this).data('user'),
                        'checked' : $(this).is(":checked") *1
                    }
                    $.post('/admin/backend/update-checked',postData);
                })
                $(document).on('click','.accept_multiple',function(){
                    bootbox.confirm("Are you sure you want to accept multiple users ?", function(result) {
                       if(result == true){
                        $("#deleteMult").ajaxSubmit({
                              url: "/admin/backend/accept-multiple", 
                              type: 'post',
                              success: acceptedSuccess,
                              error: errorFn
                        });

                        function errorFn(xhr, status, strErr){

                           msg = "All users were not accepted"	;
                                                    title = "There was an error";
                                                    theme ="error";
                                            var $toast = toastr[theme](title, msg);
                        }
                        function acceptedSuccess(data){

                            for(var i = 0; i<data.length; i++){
                                //Bad idea to use .each(), much slower in this case
                                var table = $('#sample_1').DataTable();

                                table
                                    .row( $("#data-row-"+data[i]) )
                                    .remove()
                                    .draw();

                                //$("#data-row-"+arr[i]).remove();

                            }
                          //$("#data-row-"+stuffId).remove();
                           msg = "Successfully Accepted"	;
                                                    title = "";
                                                    theme ="info";
                                            var $toast = toastr[theme](title, msg);  
                        }



                       }
                    }); 
            });
                $(document).on('click','.reject_multiple',function(){
                    bootbox.confirm("Are you sure you want to reject multiple users ?", function(result) {
                       if(result == true){
                        $("#deleteMult").ajaxSubmit({
                              url: "/admin/backend/reject-multiple", 
                              type: 'post',
                              success: rejectedSuccess,
                              error: errorFn
                        });

                        function errorFn(xhr, status, strErr){

                           msg = "All users were not rejected"	;
                                                    title = "There was an error";
                                                    theme ="error";
                                            var $toast = toastr[theme](title, msg);
                        }
                        function rejectedSuccess(data){

                            for(var i = 0; i<data.length; i++){
                                //Bad idea to use .each(), much slower in this case
                                var table = $('#sample_1').DataTable();

                                table
                                    .row( $("#data-row-"+data[i]) )
                                    .remove()
                                    .draw();

                                //$("#data-row-"+arr[i]).remove();

                            }
                          //$("#data-row-"+stuffId).remove();
                           msg = "Successfully Rejected"	;
                                                    title = "";
                                                    theme ="info";
                                            var $toast = toastr[theme](title, msg);  
                        }



                       }
                    }); 
				});
</script>
    
@stop
@section('add_inits')
	
	
@stop
@section('title')
	{{$current_user->name}}
@stop

@section('page_title')
	{{$current_user->name}}
@stop

@section('page_title_small')
	
@stop

@section('content')
<div class="table-toolbar">
<div class="row">
<form method="POST" id="search-form" class="form-inline" role="form" dir="ltr">

			<div class="form-group">
			  <input class="form-control" name="name" id="name" placeholder="Search name" type="text">
			</div>
			<div class="form-group">
				<input class="form-control" name="email" id="email" placeholder="Search email" type="text">
			</div>
            <div class="form-group">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input class="form-control datetimePicker" name="date_start" id="date_start" placeholder="Test from" type="text"></td>
  </tr>
  <tr>
    <td height="5"><div></div></td>
  </tr>
  <tr>
    <td><input class="form-control datetimePicker" name="date_end" id="date_end" placeholder="Test to" type="text"></td>
  </tr>
</table>

				
				
				
				
			</div>
            <div class="form-group">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input class="form-control datetimePicker" name="date_from" id="date_from" placeholder="Date from" type="text"></td>
  </tr>
  <tr>
    <td height="5"><div></div></td>
  </tr>
  <tr>
    <td><input class="form-control datetimePicker" name="date_to" id="date_to" placeholder="Date to" type="text"></td>
  </tr>
</table>

				
				
				
				
			</div>
            <div class="form-group">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input class="form-control" name="score_from" id="score_from" placeholder="Score from" type="text"></td>
  </tr>
  <tr>
    <td height="5"><div></div></td>
  </tr>
  <tr>
    <td><input class="form-control" name="score_to" id="score_to" placeholder="Score to" type="text"></td>
  </tr>
</table>

				
				
				
				
			</div>
            <div class="form-group">
			  
			  <select name="checked" id="checked">
			    <option value="2">All</option>
			    <option value="1">Checked</option>
			    <option value="0">Not checked</option>
		      </select>
            </div>

	  <a href="#" id="custom-search" >	<button  class="btn btn-primary">Search</button></a>
		</form>
</div>
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="javascript: taps.loadajaxpage('user/add')" >
                            <button id="sample_editable_1_new" class="btn green" style="float:none;">
                                Add New <i class="fa fa-plus"></i>
                            </button>
                        </a>
                        <button type="button" class="btn red delete_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" ><i class="fa fa-remove"></i> Delete</button>
                        <button type="button" class="btn green accept_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" > Accept</button>
                        <button type="button" class="btn default reject_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" >Reject</button>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
</div>
<div>
					<div dir="rtl">
                    <a class="toggle-vis bold" data-column="7">Checked</a> -
                    <a class="toggle-vis bold" data-column="6">Test</a> -
                    <a class="toggle-vis bold" data-column="5">Duration</a> - 
                    <a class="toggle-vis bold" data-column="4">End Time</a> - 
                    <a class="toggle-vis bold" data-column="3">Start Time</a> - 
                    <a class="toggle-vis bold" data-column="2">Score</a> - 
                    <a class="toggle-vis bold" data-column="1">Name</a>  
                    </div>
				</div>
                <form name="vpb_form_name" id="deleteMult" method="post" action="">
                {!! csrf_field() !!}
<table class="table table-condensed" id="users-table">
        <thead>
            <tr>
              <th></th>
                
              <th>Name</th>
                <th>Score</th>
                <th>Start time</th>
                <th>End Time</th>
                <th>Duration</th>
              <th width="100">Test</th>
                <th width="50">Checked</th>
                
          </tr>
        </thead>
    </table>
    </form>
@stop



