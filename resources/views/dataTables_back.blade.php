<?php $i=3; $j =2;?>
@extends('admin.master')
@section('add_css')
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
   
@stop

@section('add_js_plugins')
	<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/table-managed.js"></script>
    <script>
		
		$(document).ready(function() {
			var table = $('#users-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '/multi-filter-select-data',
			stateSave: true,
			columns: [
				{data: 'id', name: 'id'},
				{data: 'name', name: 'name'},
				{data: 'email', name: 'email'},
				{data: 'created_at', name: 'created_at'},
				{data: 'updated_at', name: 'updated_at'},
				{data: 'action', name: 'action', orderable: false, searchable: false},
				{data: 'Checked', name: 'Checked', orderable: false, searchable: false}
				
			],
			initComplete: function () {
				this.api().columns().every(function () {
					var column = this;
					var input = document.createElement("input");
					$(input).appendTo($(column.footer()).empty())
					.on('change', function () {
						column.search($(this).val(), false, false, true).draw();
					});
				});
			}
		});
		$('a.toggle-vis').on( 'click', function (e) {
			e.preventDefault();
	 
			// Get the column API object
			var column = table.column( $(this).attr('data-column') );
	 
			// Toggle the visibility
			column.visible( ! column.visible() );
		} );
			
			
		});
</script>
    
@stop
@section('add_inits')
	
	
@stop
@section('title')
	{{$current_user->name}}
@stop

@section('page_title')
	{{$current_user->name}}
@stop

@section('page_title_small')
	
@stop

@section('content')
<div>
					Toggle column: <a class="toggle-vis" data-column="0">Name</a> - <a class="toggle-vis" data-column="1">Position</a> - <a class="toggle-vis" data-column="2">Office</a> - <a class="toggle-vis" data-column="3">Age</a> - <a class="toggle-vis" data-column="4">Start date</a> - <a class="toggle-vis" data-column="5">Salary</a>
				</div>
<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
                <th>Checked</th>
                
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
                <th>Checked</th>
            </tr>
        </tfoot>
    </table>
@stop



