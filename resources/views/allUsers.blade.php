<?php 
if($_GET['status'] == 'all'){
	$i=3; $j =2;
}else{
	$i=3; $j =3;
}
?>
@extends('admin.master')
@section('add_css')
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
    <style>
	div.dataTables_wrapper div.dataTables_filter{
		display:none;
	}
	input.form-control{
		width:200px !important;
	}
	.portlet{
		-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
	}
	</style>
   
@stop

@section('add_js_plugins')
	<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/table-managed.js"></script>

    <script>
		
		$(document).ready(function() {
			var table = $('#users-table').DataTable({
				
			processing: true,
			serverSide: true,
			ajax: {
				url :'/admin/backend/multi-filter-select-data/<?php echo $_GET['status'];?>',
				data: function (d) {
                d.name = $('input[name=name]').val();
                d.email = $('input[name=email]').val();
				d.date_start = $('input[name=date_start]').val();
				d.date_end = $('input[name=date_end]').val();
				d.date_from = $('input[name=date_from]').val();
				d.date_to = $('input[name=date_to]').val();
				d.score_from = $('input[name=score_from]').val();
				d.score_to = $('input[name=score_to]').val();
				d.checked = $('#checked').val();
            }
			},
			columns: [
				{data: 'Checkbox', name: 'Checkbox', orderable: false, searchable: false},
				{data: 'name', name: 'name'},
				{data: 'test_score', name: 'test_score'},
				{data: 'start_time', name: 'start_time'},
				{data: 'end_time', name: 'end_time'},
				{data: 'duration', name: 'duration', searchable: false},
				{data: 'Test', name: 'Test', orderable: false, searchable: false},
				{data: 'Checked', name: 'Checked', orderable: false, searchable: false}
			],
			
			initComplete: function () {
				var checkboxes = $("input[type='checkbox']"),
				submitButt = $(".multiple-controls");
				checkboxes.click(function() {
				submitButt.attr("disabled", !checkboxes.is(":checked"));
			});
			}
		});
		
		$("#search-form").submit(function(e){
       	 e.preventDefault();
		 table.draw();
    	});
		$('a.toggle-vis').on( 'click', function (e) {
			e.preventDefault();
	 		$(this).toggleClass('bold');
			// Get the column API object
			var column = table.column( $(this).attr('data-column') );
	 
			// Toggle the visibility
			column.visible( ! column.visible() );
		} );
			
			
		});
</script>

<script type="text/javascript">


                $(document).on('change','.update-checked',function (){
                    postData = {
                        '_token': '{{csrf_token()}}',
                        'user_id': $(this).data('user'),
                        'checked' : $(this).is(":checked") *1
                    }
                    $.post('/admin/backend/update-checked',postData);
                })
				$('.delete_multiple').click(function(){
                
                modelName = $(this).attr('data-model');
           
                bootbox.confirm("Are you sure you want to delete multiple items ?", function(result) {
                   if(result == true){
                    $("#deleteMult").ajaxSubmit({
                          url: "/admin/delete/multiple/"+modelName+"", 
                          type: 'post',
                          success: deleteSuccess,
                          error: errorFn
                    });
                   
                    function errorFn(xhr, status, strErr){
		             
                       msg = "Nothing Deleted"	;
						title = "There was an error";
						theme ="error";
					var $toast = toastr[theme](title, msg);
                    }
                    function deleteSuccess(data){
                        var arr = data.split(',');

                        for(var i = 0; i<arr.length; i++){
                            //Bad idea to use .each(), much slower in this case
                            var table = $('#users-table').DataTable();

                            table
                                .row( $("#data-row-"+arr[i]) )
                                .remove()
                                .draw();
                            
                            //$("#data-row-"+arr[i]).remove();
                            
                        }
                      //$("#data-row-"+stuffId).remove();
                       msg = "Successfully Deleted"	;
						title = "";
						theme ="info";
					var $toast = toastr[theme](title, msg);  
                    }
                    
                    
                       
                   }
                }); 
            });
                $(document).on('click','.accept_multiple',function(){
                    bootbox.confirm("Are you sure you want to accept multiple users ?", function(result) {
                       if(result == true){
                        $("#deleteMult").ajaxSubmit({
                              url: "/admin/backend/accept-multiple", 
                              type: 'post',
                              success: acceptedSuccess,
                              error: errorFn
                        });

                        function errorFn(xhr, status, strErr){

                           msg = "All users were not accepted"	;
                                                    title = "There was an error";
                                                    theme ="error";
                                            var $toast = toastr[theme](title, msg);
                        }
                        function acceptedSuccess(data){

                            for(var i = 0; i<data.length; i++){
                                //Bad idea to use .each(), much slower in this case
                                var table = $('#sample_1').DataTable();

                                table
                                    .row( $("#data-row-"+data[i]) )
                                    .remove()
                                    .draw();

                                //$("#data-row-"+arr[i]).remove();

                            }
                          //$("#data-row-"+stuffId).remove();
                           msg = "Successfully Accepted"	;
                                                    title = "";
                                                    theme ="info";
                                            var $toast = toastr[theme](title, msg);  
                        }



                       }
                    }); 
            });
                $(document).on('click','.reject_multiple',function(){
                    bootbox.confirm("Are you sure you want to reject multiple users ?", function(result) {
                       if(result == true){
                        $("#deleteMult").ajaxSubmit({
                              url: "/admin/backend/reject-multiple", 
                              type: 'post',
                              success: rejectedSuccess,
                              error: errorFn
                        });

                        function errorFn(xhr, status, strErr){

                           msg = "All users were not rejected"	;
                                                    title = "There was an error";
                                                    theme ="error";
                                            var $toast = toastr[theme](title, msg);
                        }
                        function rejectedSuccess(data){

                            for(var i = 0; i<data.length; i++){
                                //Bad idea to use .each(), much slower in this case
                                var table = $('#sample_1').DataTable();

                                table
                                    .row( $("#data-row-"+data[i]) )
                                    .remove()
                                    .draw();

                                //$("#data-row-"+arr[i]).remove();

                            }
                          //$("#data-row-"+stuffId).remove();
                           msg = "Successfully Rejected"	;
                                                    title = "";
                                                    theme ="info";
                                            var $toast = toastr[theme](title, msg);  
                        }



                       }
                    }); 
				});
</script>
    
@stop
@section('add_inits')
	
	
@stop
@section('title')
	{{$current_user->name}}
@stop

@section('page_title')
	{{$current_user->name}}
@stop

@section('page_title_small')
	
@stop

@section('content')
<div class="table-toolbar">

<div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-search"></i>
                                        <span class="caption-subject bold uppercase"> Search</span>
                                        <span class="caption-helper"></span>
                                    </div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form method="POST" id="search-form" class="form-inline" role="form" dir="ltr">

			<div class="form-group">
			  <input class="form-control" name="name" id="name" placeholder="Search name" type="text">
			</div>
			<div class="form-group">
				<input class="form-control" name="email" id="email" placeholder="Search email" type="text">
			</div>
            <div class="form-group">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input class="form-control datetimePicker" name="date_start" id="date_start" placeholder="Test from" type="text"></td>
  </tr>
  <tr>
    <td height="5"><div></div></td>
  </tr>
  <tr>
    <td><input class="form-control datetimePicker" name="date_end" id="date_end" placeholder="Test to" type="text"></td>
  </tr>
</table>

				
				
				
				
			</div>
            <div class="form-group">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input class="form-control datetimePicker" name="date_from" id="date_from" placeholder="Date from" type="text"></td>
  </tr>
  <tr>
    <td height="5"><div></div></td>
  </tr>
  <tr>
    <td><input class="form-control datetimePicker" name="date_to" id="date_to" placeholder="Date to" type="text"></td>
  </tr>
</table>

				
				
				
				
			</div>
            <div class="form-group">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input class="form-control" name="score_from" id="score_from" placeholder="Score from" type="text"></td>
  </tr>
  <tr>
    <td height="5"><div></div></td>
  </tr>
  <tr>
    <td><input class="form-control" name="score_to" id="score_to" placeholder="Score to" type="text"></td>
  </tr>
</table>

				
				
				
				
			</div>
            <div class="form-group">
			  
			  <select name="checked" id="checked">
			    <option value="2">All</option>
			    <option value="1">Checked</option>
			    <option value="0">Not checked</option>
		      </select>
            </div>

	  <a href="#" id="custom-search" >	<button  class="btn btn-primary">Search</button></a>
		</form>
                                </div>
                            </div>


<div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>
                                        <span class="caption-subject bold uppercase"> Transcripers</span>
                                        <span class="caption-helper"></span>
                                    </div>
                                    <div class="actions">
                                                                            

                    <a class="toggle-vis bold" data-column="7">Checked</a> -
                    <a class="toggle-vis bold" data-column="6">Test</a> -
                    <a class="toggle-vis bold" data-column="5">Duration</a> - 
                    <a class="toggle-vis bold" data-column="4">End Time</a> - 
                    <a class="toggle-vis bold" data-column="3">Start Time</a> - 
                    <a class="toggle-vis bold" data-column="2">Score</a> - 
                    <a class="toggle-vis bold" data-column="1">Name</a>  
                    
                                        <div class="btn-group">
                                        <a href="/admin/backend/addUser" ><button type="button" class="btn blue  multiple-controls"  style="float:none;"  ><i class="fa fa-plus"></i> Add new</button></a>
                                          <button type="button" class="btn red delete_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" ><i class="fa fa-remove"></i> Delete</button>
                                            <button type="button" class="btn green accept_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" > Accept</button>
                                            <button type="button" class="btn default reject_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" >Reject</button>
                                    	</div>
                                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                                </div>
                                <div class="portlet-body">
                                
                                        
                                    <form name="vpb_form_name" id="deleteMult" method="post" action="">
                {!! csrf_field() !!}
<table class="table table-condensed" id="users-table">
        <thead>
            <tr>
              <th></th>
                
              <th>Name</th>
                <th>Score</th>
                <th>Start time</th>
                <th>End Time</th>
                <th>Duration</th>
              <th width="100">Test</th>
                <th width="50">Checked</th>
                
          </tr>
        </thead>
    </table>
    </form>
    
                                </div>
                            </div>
            
</div>
<div>
					
				</div>
                
@stop



