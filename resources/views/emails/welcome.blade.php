<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body style="border:2px solid #BD4949; padding:15px; margin:7px;">
<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle"><img src="http://arabiclocalizer.com/images/logo.jpg" style="max-width:100%;" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top" bgcolor="#BD4949"><div></div></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="right" valign="top" style="font-size:120%;"><p style="font-size:120%;" dir="rtl"><strong> عزيزي/ عزيزتي، {{$user->name}}</strong><br />
      خالص الشكر  للتقديم على وظيفة مدخل بيانات. نود إخباركم بأنه تم قبولك للالتحاق بالتدريب على مشروع تابع لجوجل  فويس (<span dir="ltr">Google Voice</span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span>) وهو عبارة عن نظام بحث عبر الإنترنت باستخدام  البحث الصوتي بجوجل، ولضمان نتائج بحث عالية الجودة تم تصميم هذا المشروع من خلال تعيين  مدخلي بيانات للعمل على إدخال عدد جمل معينة مطابقة للأبحاث الصوتية  لمستخدمي محرك البحث جوجل لضمان نتائج بحث فائقة الجودة<br />
      للدخول على الموقع الخاص بالتدريب والاختبار، رجاء الضغط على الرابط  التالي:</p>
      </br>
      <a href="http://transcription.arabiclocalizer.info/admin/training" > أضغط هنا </a>
      </br>
    <span dir="rtl">وهذا  هو شكل الموقع الذي سيظهر أمامك.</span></td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="http://transcription.arabiclocalizer.info/images/1.PNG" style="max-width:50%;" /></td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="right" valign="top" style="font-size:120%;"><p dir="rtl">والتالي هو اسم المستخدم وكلمة المرور الخاصة بك، والتي ستمكنك من الدخول إلى  حسابك على هذا الموقع التدريبي:<span dir="ltr"> </span></p>
    <br>
    <span dir="rtl">البريد الاكتروني : {{$user->email}}</span>
    <br>
    <span dir="rtl">كلمة المرور : {{$user->dec_password}}</span>
    <br>
    <span dir="rtl">يمكنك  الضغط على القسم الثاني في الموقع (مادة التدريب) :</span></td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="http://transcription.arabiclocalizer.info/images/2.PNG" alt="" style="max-width:50%;" /></td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="right" valign="top" style="font-size:120%;"><p dir="rtl"><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span> لتشاهد فيديوهات التدريب. والتي ستقوم بشرح  كيفية استخدام برنامج بيرا بيرا (<span dir="ltr">PeraPera</span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span>) وهو البرنامج المسئول عن  تنظيم إدخال البيانات. كما ستقوم هذه الفيديوهات بسرد أنواع التسجيلات والحالات  التي ستواجهك من التسجيلات والمطلوب منك تفريغها.<br />
    بعد مشاهدة  فيديوهات التدريب جيداً، يمكنك الضغط على (الاختبار)<span dir="ltr"> </span><span dir="ltr"> </span><span dir="ltr"><span dir="ltr"> </span><span dir="ltr"> </span>:</span></p></td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="http://transcription.arabiclocalizer.info/images/3.PNG" alt="" style="max-width:50%;" /></td>
  </tr>
  <tr>
    <td align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="right" valign="top" style="font-size:120%;"><p dir="rtl">كي تدخل الامتحان  الذي سيحدد قبولك في الوظيفة أم استبعادك. سيكون هذا الامتحان فعال يوم السبت  القادم الموافق: 6-8-2016<br />
      <strong>تفاصيل الامتحان:</strong></p>
      <ul dir="rtl">
        <li><span dir="rtl"> </span>قبل دخول الامتحان تأكد من أنك متصل  بالإنترنت وأن سرعته جيدة<span dir="ltr"> </span></li>
        <li><span dir="rtl"> </span>لك فرصة واحدة لدخول الامتحان والنجاح فيه<span dir="ltr"> </span></li>
        <li><span dir="rtl"> </span>الامتحان يتكون من <span dir="ltr"> </span><span dir="ltr"> </span><span dir="ltr"><span dir="ltr"> </span><span dir="ltr"> </span>200</span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span> تسجيل صوتي  يتطلب منك اختيار الإجابة الصحيحة لكل تسجيل أو كتابته<span dir="ltr"> </span></li>
        <li><span dir="rtl"> </span>مدة الامتحان ساعتين<span dir="ltr"> </span></li>
      </ul>
      <p dir="rtl"><strong>بالنسبة للناجحين  في اختبار التعيين:</strong></p>
      <ol dir="rtl">
        <li><span dir="rtl"> </span>نقوم بإضافة اسمك وحسابك في قاعدة البيانات  الخاصة بالناجحين حيث ينسق مدير المشروع العمل الذي ستكون مسئولا عنه.<span dir="ltr"> </span></li>
        <li><span dir="rtl"> </span>يتم دفع مبلغ 100 جنيه على كل<span dir="ltr"> </span><span dir="ltr"> </span><span dir="ltr"><span dir="ltr"> </span><span dir="ltr"> </span> 1000 </span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span><span dir="rtl"> </span> جملة يتم تفريغها على حسب توزيع مدير المشروع  الخاص بـ<span dir="ltr">Arabic Localizer</span></li>
        <li><span dir="rtl"> </span>يتم إختيار نظام إستلام المرتب من بين  التالى:<span dir="ltr"> </span></li>
        <ul>
          <li><span dir="rtl"> </span>كل أسبوع<span dir="ltr"> </span></li>
          <li><span dir="rtl"> </span>كل أسبوعين<span dir="ltr"> </span></li>
          <li><span dir="rtl"> </span>كل شهر<span dir="ltr"> </span></li>
        </ul>
        <li><span dir="rtl"> </span>يتم استلام الراتب من عناوين مكتب<span dir="ltr"> </span><span dir="ltr"> </span><span dir="ltr"><span dir="ltr"> </span><span dir="ltr"> </span> Arabic Localizer </span>في القاهرة أو الأسكندرية:-<span dir="ltr"> </span></li>
        <li><span dir="rtl"> </span>يرجى متابعة الإيميل بشكل دورى لمعرفة أى  مستجدات بالمشروع<span dir="ltr"> </span></li>
      </ol>
      <p dir="rtl">وتفضل بقبول فائق  الاحترام</p>
    <span dir="rtl">فريق  العمل بمشروع إدخال البيانات</span></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top" bgcolor="#BD4949"><div></div></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">Arabic Localizer © 2016</td>
  </tr>
</table>
</body>
</html>