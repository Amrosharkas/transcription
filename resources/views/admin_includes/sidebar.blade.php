<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
					<form class="sidebar-search " action="extra_search.html" method="POST" style="display:none;">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
                <li class="heading">
					<h3 class="uppercase"></h3>
				</li>
                @if($current_user->user_type =='Transcriber')
				<li class="<?php if($i==1){?>active open<?php ;}?>">
					<a href="javascript:;">
					<i class="fa fa-edit"></i>
					<span class="title">Transcription (مدخل بيانات)</span>
					<span <?php if($i==1){?>class="selected"<?php ;}?>></span>
					<span class="arrow <?php if($i==1){?>open<?php ;}?>"></span>
					</a>
					<ul class="sub-menu">
                    	
                            <li <?php if($i==1 && $j==2){?>class="active"<?php ;}?>>
                                <a href="/admin/users">
                                حسابي</a>
                            </li>
                            <li <?php if($i==1 && $j==3){?>class="active"<?php ;}?>>
                                <a href="/admin/training">
                                مادة التدريب</a>
                            </li>
                            @if($current_user->status !='accepted')
                        <li <?php if($i==1 && $j==4){?>class="active"<?php ;}?>>
                        <a href="/admin/test" class="nav-link ">
                                        <span class="title">الاختبار</span>
                                      <?php if(!isset($current_user->getUserTest->status) ){?> <span class="badge badge-danger">1</span><?php ;}?>
                                    </a>
							
						</li>
                        	@endif
					</ul>
				</li>
                @endif()
                @if($current_user->status =='accepted')
                <li class="<?php if($i==2){?>active open<?php ;}?>">
					<a href="javascript:;">
					<i class="fa fa-eye"></i>
					<span class="title">Productivity</span>
					<span <?php if($i==2){?>class="selected"<?php ;}?>></span>
					<span class="arrow <?php if($i==1){?>open<?php ;}?>"></span>
					</a>
					<ul class="sub-menu">
                    	
                            <li <?php if($i==2 && $j==1){?>class="active"<?php ;}?>>
                                <a href="/admin/payments">
                                Payments / Balance</a>
                            </li>
					</ul>
				</li>
                @endif()
                @if($current_user->user_type =='Prince')
                <li class="<?php if($i==3){?>active open<?php ;}?>">
					<a href="javascript:;">
					<i class="fa fa-user"></i>
					<span class="title">Transcribers</span>
					<span <?php if($i==3){?>class="selected"<?php ;}?>></span>
					<span class="arrow <?php if($i==1){?>open<?php ;}?>"></span>
					</a>
					<ul class="sub-menu">
					  <li <?php if($i==3 && $j==2){?>class="active"<?php ;}?>>
                          <a href="/admin/backend/allUsers?status=all">
                              All Candidates</a>
                        </li>
                            <li <?php if($i==3 && $j==3){?>class="active"<?php ;}?>>
                                <a href="/admin/backend/allUsers?status=new">
                                    New Candidates</a>
                            </li>
                            <li class="accepted">
                                <a href="/admin/backend/accepted">
                                    Accepted</a>
                            </li>
                            <li class="rejected">
                                <a href="/admin/backend/rejected">
                                    Rejected</a>
                            </li>
                             <li class="dismissed">
                                <a href="/admin/backend/dismissed">
                                    Dismissed</a>
                            </li>
					</ul>
				</li>
                @endif()
                @if($current_user->user_type =='Prince')
                <li class="<?php if($i==4){?>active open<?php ;}?>">
					<a href="javascript:;">
					<i class="fa fa-folder"></i>
					<span class="title">Project Management</span>
					<span <?php if($i==4){?>class="selected"<?php ;}?>></span>
					<span class="arrow <?php if($i==4){?>open<?php ;}?>"></span>
					</a>
					<ul class="sub-menu">
                    	
                            <li <?php if($i==4 && $j==1){?>class="active"<?php ;}?>>
                                <a href="/admin/backend/projects/active">
                                    Active Projects</a>
                            </li>
                            <li <?php if($i==4 && $j==3){?>class="active"<?php ;}?>>
                                <a href="/admin/backend/projects/completed">
                                    Completed Projects</a>
                            </li>  
                            <li <?php if($i==4 && $j==2){?>class="active"<?php ;}?>>
                                <a href="/admin/backend/pera_peras">
                                    Pera Pera Accounts</a>
                            </li> 
					</ul>
				</li>
                @endif()
				
		  </ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>