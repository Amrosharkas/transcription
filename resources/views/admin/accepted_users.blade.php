
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>



        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
        </div>
        <form name="vpb_form_name" id="deleteMult" method="post" action="">
            {!! csrf_field() !!}
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr class="tr-head">
                        <th class="table-checkbox">
                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Mobile
                        </th>
                        <th>
                            City
                        </th>
                        <th>
                            Score
                        </th>
                        <th width="400">Notes</th>
                        <th width="300">Pera_pera</th>
                        <th width="400">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr class="odd gradeX" id="data-row-{{$user->id}}">
                        <td valign="middle">

                            <input type="checkbox" name="users[]"  class="checkboxes"  value="{{$user->id}}" >
                        </td>
                        <td valign="middle">
                            {{$user->name}}
                        </td>
                        <td valign="middle">
                            {{$user->email}}
                        </td>
                        <td valign="middle">
                            {{$user->mobile}}
                        </td>
                        <td valign="middle">
                            {{$user->city}}
                        </td>
                        <td valign="middle">
                            {{$user->test_score}}
                        </td>
                        <td width="400" valign="middle">
                        <?php if($user->user_notes){?>
                        {{$user->user_notes}} - <a href="javascript: taps.loadajaxpage('user/add_note/{{$user->id}}')" style="float:left;" > 
                        	<button type="button" class="btn blue"><i class="fa fa-edit"></i> </button>
                         </a>
                        <?php ;}else{?>
                        <a href="javascript: taps.loadajaxpage('user/add_note/{{$user->id}}')" style="float:left;"> 
                        	<button type="button" class="btn blue"><i class="fa fa-edit"></i> Add note</button>
                        </a>
                        <?php ;}?>
                        </td>
                        <td width="300" valign="middle"><?php if($user->pera_email == ""){?>
                          <a href="javascript: taps.loadajaxpage('pera/assign/{{$user->id}}')">
                            <button type="button" class="btn green"><i class="fa fa-edit"></i> Assign</button>
                          </a>
                          <?php ;}else{?>
                          <a title="Detach" href="javascript: taps.loadajaxpage('pera/{{$user->id}}/detach')">
                            <button type="button" class="btn grey-mint"><i class="fa fa-remove"></i>
                              <?php $parts = explode("@",$user->pera_email);
												echo $parts[0];
												?>
                          </button>
                          </a>
                          <?php ;}?></td>
                        <td width="400" valign="middle">
                        <a title="Dismiss" href="javascript: taps.loadajaxpage('/admin/dismiss/{{$user->id}}')"><button type="button" class="btn red" ><i class="fa fa-remove"></i> </button></a>
                        <a title="Payments" href="javascript: taps.loadajaxpage('payments/{{$user->id}}')">
                          <button type="button" class="btn green-meadow"><i class="fa fa-usd"></i> </button>
                        </a>
                        <a href="/admin/backend/{{$user->id}}/results" target="_blank"><button type="button" class="btn green-seagreen">Test</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>
