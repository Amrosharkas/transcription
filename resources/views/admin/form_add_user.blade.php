<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/backend/doAddUser" method="post" class="j-forms" id="j-forms" dir="rtl" novalidate>

		{!! csrf_field() !!}

		  <div class="content">

				<!-- start name -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="name">
								<i class="fa fa-user"></i>
							</label>
							<input type="text" id="name" name="name"  >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">الاسم</label>
					</div>
				</div>
				<!-- end name -->

				<!-- start email -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="email" >
								<i class="fa fa-envelope-o"></i>
							</label>
                          
							<input type="email" id="email" name="email" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">البريد الالكتروني</label>
					</div>
				</div>
				<!-- end email -->

				<!-- start password --><!-- end password -->
				<!-- start Mobile -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="mobile">
								<i class="fa fa-phone"></i>
							</label>
							<input type="text" id="mobile" name="mobile"  >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">المحمول</label>
					</div>
			</div>
				<!-- end Mobile -->
				<!-- start City -->
			  <div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="city">
								<i class="fa fa-map-marker"></i>
							</label>
							<input type="text" id="city" name="city"  >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">المدينة</label>
					</div>
				</div>
				<!-- end City -->
                
			<!-- start response from server -->
			  <div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >Submit</button>
			</div>
			<!-- end /.footer -->

		</form>
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					