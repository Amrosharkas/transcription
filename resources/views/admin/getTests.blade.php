<p dir="rtl"><strong><u>رجاء الإنتباه للتالى قبل الدخول فى الإختبار:</u></strong><strong><u><span dir="ltr"> </span></u></strong></p>
<ul>
  <li><span dir="rtl"> </span>معرفة أن الوقت المحدد للاختبار هو ساعتان فقط<span dir="ltr"> </span></li>
  <li><span dir="rtl"> </span>بمجرد الضغط على الزر المخصص للاختبار سوف يتم احتساب الوقت و لن يتم إعادة  الاختبار مرة أخرى بمجرد الإنتهاء منه<span dir="ltr"> </span></li>
  <li><span dir="rtl"> </span>فى عدم الاجابة على السؤال تعتير الإجابة خطأ<span dir="ltr"> </span></li>
  <li><span dir="rtl"> </span>لن يتم الرجوع للسؤال مرة أخرى بمجرد الإجابة عليه و الضغط على التالى</li>
</ul>
<a href="javascript: taps.loadajaxpage('/admin/generateTest/{{$current_user->id}}')" > <button class="btn btn-info" >ابدأ الاختبار</button> </a>
