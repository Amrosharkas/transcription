
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>



        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="javascript: taps.loadajaxpage('/admin/backend/pera_peras/add/0')" >
                            <button id="sample_editable_1_new" class="btn green" style="float:none;">
                                Add New <i class="fa fa-plus"></i>
                            </button>
                        </a>
                        <button type="button" class="btn red delete_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" ><i class="fa fa-remove"></i> Delete</button>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <form name="vpb_form_name" id="deleteMult" method="post" action="">
            {!! csrf_field() !!}
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                <tr>
                <th class="table-checkbox">
                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
                </th>
                <th>
                    Email
                </th>
                <th>
                    Password
                </th>
                <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($pera_peras as $pera_pera)
                    <tr class="odd gradeX" id="data-row-{{$pera_pera->id}}">
                        <td valign="middle">

                            <input type="checkbox" name="users[]"  class="checkboxes"  value="{{$pera_pera->id}}" >
                        </td>
                        <td valign="middle">
                            {{$pera_pera->pera_email}}
                        </td>
                        <td valign="middle">
                            {{$pera_pera->password}}
                        </td>
                        <td valign="middle">
                             
                             <a href="javascript: taps.loadajaxpage('/admin/backend/pera_pera/{{$pera_pera->id}}')"><button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button></a>
                             <button type="button" id="{{$pera_pera->id}}" data-model="{{$model_name}}" class="btn red delete"><i class="fa fa-remove"></i> Delete</button>
                             </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>