<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/backend/pera_peras/save" method="post" class="j-forms" id="j-forms" dir="rtl" novalidate>

 @if(isset($pera_pera)) <input type="hidden" name="id" id="id" value="{{$pera_pera->id}}" />@endif
		{!! csrf_field() !!}

			<div class="content">

				<!-- start name --><!-- end name -->
@if(!isset($pera_pera->pera_email))
				<!-- start email -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="email" >
								<i class="fa fa-envelope-o"></i>
							</label>
                          
							<input type="email" id="email" name="pera_email"  @if(isset($pera_pera))value="{{$pera_pera->pera_email}}"@endif>
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">البريد الالكتروني</label>
					</div>
				</div>
				<!-- end email -->
@endif
				<!-- start password -->
			  <div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="password">
								<i class="fa fa-lock"></i>
							</label>
							<input type="text" id="password" name="password" @if(isset($pera_pera))value="{{$pera_pera->password}}"@endif>
					      
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">كلمة المرور</label>
					</div>
				</div>
				<!-- end password -->
				<!-- start Mobile --><!-- end Mobile -->
				<!-- start City --><!-- end City -->

				<!-- start response from server -->
			  <div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >تحديث</button>
			</div>
			<!-- end /.footer -->

		</form>
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					