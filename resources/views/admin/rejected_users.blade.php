
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>



        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
        </div>
        <form name="vpb_form_name" id="deleteMult" method="post" action="">
            {!! csrf_field() !!}
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr class="tr-head">
                        <th class="table-checkbox">
                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Mobile
                        </th>
                        <th>
                            City
                        </th>
                        <th>
                            Score
                        </th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr class="odd gradeX" id="data-row-{{$user->id}}">
                        <td valign="middle">

                            <input type="checkbox" name="users[]"  class="checkboxes"  value="{{$user->id}}" >
                        </td>
                        <td valign="middle">
                            {{$user->name}}
                        </td>
                        <td valign="middle">
                            {{$user->email}}
                        </td>
                        <td valign="middle">
                            {{$user->mobile}}
                        </td>
                        <td valign="middle">
                            {{$user->city}}
                        </td>
                        <td valign="middle">
                            {{$user->test_score}}
                        </td>
                        <td valign="middle">&nbsp;</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>
