@extends('admin.master')
<?php $i=1; $j =4;?>
@section('add_css')
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="/css/style.css"/>
    
    <style>
.funkyradio div {
  clear: both;
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 2.5em;
  text-indent: 3.25em;
  margin-top: 2em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #777;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}

.funkyradio-danger input[type="radio"]:checked ~ label:before,
.funkyradio-danger input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #d9534f;
}

.funkyradio-warning input[type="radio"]:checked ~ label:before,
.funkyradio-warning input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #f0ad4e;
}

.funkyradio-info input[type="radio"]:checked ~ label:before,
.funkyradio-info input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5bc0de;
}

/* SCSS STYLES */
/*
.funkyradio {

    div {
        clear: both;
        overflow: hidden;
    }

    label {
        width: 100%;
        border-radius: 3px;
        border: 1px solid #D1D3D4;
        font-weight: normal;
    }

    input[type="radio"],
    input[type="checkbox"] {

        &:empty {
            display: none;

            ~ label {
                position: relative;
                line-height: 2.5em;
                text-indent: 3.25em;
                margin-top: 2em;
                cursor: pointer;
                user-select: none;

                &:before {
                    position: absolute;
                    display: block;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    content: '';
                    width: 2.5em;
                    background: #D1D3D4;
                    border-radius: 3px 0 0 3px;
                }
            }
        }

        &:hover:not(:checked) ~ label {
            color: #888;

            &:before {
                content: '\2714';
                text-indent: .9em;
                color: #C2C2C2;
            }
        }

        &:checked ~ label {
            color: #777;

            &:before {
                content: '\2714';
                text-indent: .9em;
                color: #333;
                background-color: #ccc;
            }
        }

        &:focus ~ label:before {
            box-shadow: 0 0 0 3px #999;
        }
    }

    &-default {
        input[type="radio"],
        input[type="checkbox"] {
            &:checked ~ label:before {
                color: #333;
                background-color: #ccc;
            }
        }
    }

    &-primary {
        input[type="radio"],
        input[type="checkbox"] {
            &:checked ~ label:before {
                color: #fff;
                background-color: #337ab7;
            }
        }
    }

    &-success {
        input[type="radio"],
        input[type="checkbox"] {
            &:checked ~ label:before {
                color: #fff;
                background-color: #5cb85c;
            }
        }
    }

    &-danger {
        input[type="radio"],
        input[type="checkbox"] {
            &:checked ~ label:before {
                color: #fff;
                background-color: #d9534f;
            }
        }
    }

    &-warning {
        input[type="radio"],
        input[type="checkbox"] {
            &:checked ~ label:before {
                color: #fff;
                background-color: #f0ad4e;
            }
        }
    }

    &-info {
        input[type="radio"],
        input[type="checkbox"] {
            &:checked ~ label:before {
                color: #fff;
                background-color: #5bc0de;
            }
        }
    }
}
*/
/* Customization Style of SyoTimer */
        .timer{
            text-align: center;

            margin: 30px auto 0;
            padding: 0 0 10px;

            border-bottom: 2px solid #80a3ca;
        }
        .timer .table-cell{
            display: inline-block;
            margin: 0 5px;

            width: 79px;
            background: url(../demos/images/timer.png) no-repeat 0 0;
        }
        .timer .table-cell .tab-val{
            font-size: 35px;
            color: #80a3ca;

            height: 81px;
            line-height: 81px;

            margin: 0 0 5px;
        }
        .timer .table-cell .tab-unit{
            font-family: Arial, serif;
            font-size: 12px;
            text-transform: uppercase;
        }

        #simple_timer.timer .table-cell.day,
        #periodic_timer_days.timer .table-cell.hour{
            width: 120px;
            background-image: url(../demos/images/timer_long.png);
        }
		.day,.second{
			display:none !important;
		}
		.timer {
    font-family: "Segment7Standard";
    font-size: 30px;
    display: inline-block;
    vertical-align: top;
	margin:0 !important;
	padding:0 !important;
}
.clock {
    margin-top: 0  !important ;
}
.days {
  float: left;
  margin-right: 4px;
}
.hours {
  float: left;
}
.minutes {
  float: left;
}
.seconds {
  float: left;
}
.clearDiv {
  clear: both;
}

	</style>
@stop

@section('add_js_plugins')
	<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script> 
    <script src="/js/countdown/jquery.simple.timer.js"></script> 

@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/table-managed.js"></script>
	<script type="text/javascript">
	
	
		/***************************************/
		/* After ajax is loaded  */
		/***************************************/
		taps.onajaxpageload=function(pageurl){
			if ( $( ".timer" ).length ) {
 			$(".badge-danger").hide();
    
 

			$.getScript("/assets/admin/pages/scripts/ajax-forms.js");
			//alert(year + "-" + month + "-" + day + " " + hour + ":" + mintue);
			// prepare the form when the DOM is ready 
			$(function(){
				$('.timer').startTimer({
				  onComplete: function(){
       				 taps.loadajaxpage('/admin/endTest/<?php if(isset($current_user->getUserTest->user_id)){?>{{$current_user->getUserTest->id}}<?php ;}?>')
				  }
				});
			})
			// timer script -----------------------------------------
			}

		
			// end timer script ----------------
			$('.answers').change(function() {
				$("#submit_butt").show();
				$("#answer_type").val(this.value);
				if (this.value == 'Text') {
					$("#text_answer").show();
					$("#text_answer").focus();
					$("#text_type").hide();
					
					
				}
				else  {
					$("#text_answer").val('');
					$("#answer_text").val('');
					$("#text_answer").hide();
					$("#text_type").show();
					
				}
			});
			$("#text_answer").change(function(){
				$("#answer_text").val(this.value);
			});

			
			/***************************************/
			/* Inistantiate data tables */
			/***************************************/
			TableManaged.init();
			/***************************************/
			/* Inistantiate Modal plugin to confirm deleting */
			/***************************************/
			UIAlertDialogApi.init();

		}
	</script>
@stop
@section('add_inits')
	
	
@stop
@section('title')
الاختبار
@stop

@section('page_title')
 الاختبار
@stop

@section('page_title_small')
	
@stop

@section('content')

<div id="countrytabs" class="shadetabs" style="display:none;" >
    <li><a href="/admin/checkTest/<?php if(isset($current_user->getUserTest->id)){?>{{$current_user->getUserTest->id}}<?php ;}else{?>0<?php ;}?>/ajax" rel="countrycontainer">All users</a></li>
</div>

@stop



