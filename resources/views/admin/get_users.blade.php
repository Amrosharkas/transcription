
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>



        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="javascript: taps.loadajaxpage('user/add')" >
                            <button id="sample_editable_1_new" class="btn green" style="float:none;">
                                Add New <i class="fa fa-plus"></i>
                            </button>
                        </a>
                        <button type="button" class="btn red delete_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" ><i class="fa fa-remove"></i> Delete</button>
                        <button type="button" class="btn green accept_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" > Accept</button>
                        <button type="button" class="btn default reject_multiple multiple-controls" disabled="disabled" style="float:none;" data-model="{{$model_name}}" >Reject</button>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <form name="vpb_form_name" id="deleteMult" method="post" action="">
            {!! csrf_field() !!}
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr class="tr-head">
                    <tr>
                        Total users: {{$stats['total']}}<br>
                Total users started test: {{$stats['total_started']}}<br>
                Total users ended test: {{$stats['total_ended']}}<br>
                </tr>
                <th class="table-checkbox">
                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
                </th>
                <th>
                    Name
                </th>
                <th>
                    Score
                </th>
                <th>
                    Start Time
                </th>
                <th>
                    End Time
                </th>
                <th>
                    Duration
                </th>
                <th>
                    Results
                </th>
                <th>
                    Checked
                </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr class="odd gradeX" id="data-row-{{$user->id}}">
                        <td valign="middle">

                            <input type="checkbox" name="users[]"  class="checkboxes"  value="{{$user->id}}" >
                        </td>
                        <td valign="middle">
                            <a href="/admin/backend/{{$user->id}}/details" target="_blank">{{$user->name}}</a>
                        </td>
                        <td valign="middle">
                            {{$user->test_score}}
                        </td>
                        </td>
                        <td valign="middle">
                            {{$user->start_time}}
                        </td>
                        </td>
                        <td valign="middle">
                            {{$user->end_time}}
                        </td>
                        </td>
                        <td valign="middle">
                            {{$user->duration}}
                        </td>
                        <td>
                            @if($user->end_time)
                            <a href="/admin/backend/{{$user->id}}/results" target="_blank">View</a>
                            @endif
                        </td>
                        <td>
                            <input type="checkbox" @if($user->checked) checked @endif class="update-checked" data-user="{{$user->id}}"> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>