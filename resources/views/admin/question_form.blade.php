<div class="portlet box green output">
						<div class="portlet-title">
							<div class="caption">
								<audio preload="auto" controls autoplay>
                    <source src="/sessions/test/{{$auido_file}}" >
                </audio>
							</div>
							<div class="tools">
								سؤال <strong>{{$question->q_order}}</strong> من <strong>150</strong>
							</div>
						</div>
						<div class="portlet-body form">
							  <div class="form-actions">
                <div class="row">              
<div class="col-md-12">
     <h2>اكتب ما تسمع أو اختر الإجابة الصحيحة</h2>

    <div class="funkyradio">
        <div class="funkyradio-success">
            <input type="radio" name="radio" id="radio1" class="answers" value="Text" checked="checked" />
            <label for="radio1"><span id="text_type" style="display:none;">Type your text</span>
            <input type="text" class="form-control" id="text_answer"   /></label>
        </div>
        <div class="funkyradio-success">
            <input type="radio" name="radio" id="radio2" class="answers" value="Skip" />
            <label for="radio2">Skip</label>
        </div>
        <div class="funkyradio-success">
            <input type="radio" name="radio" id="radio3" class="answers" value="Overlapping" />
            <label for="radio3">Overlapping</label>
        </div>
        <div class="funkyradio-success">
            <input type="radio" name="radio" id="radio4" class="answers" value="Music" />
            <label for="radio4">Music</label>
        </div>
        <div class="funkyradio-success">
            <input type="radio" name="radio" id="radio5" class="answers" value="Empty" />
            <label for="radio5">Empty</label>
        </div>
    </div>
</div>
</div>

								<div class="row">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle">
    <?php $end_time  = date("Y-m-d H:i:s", strtotime($question->getOriginalTest->start_time.' +2 hours'));?>
    <div id="start_time_data" data-year="<?php echo date("Y",strtotime($end_time));?>" data-month="<?php echo date("m",strtotime($end_time));?>" data-day="<?php echo date("d",strtotime($end_time));?>" data-hour="<?php echo date("H",strtotime($end_time));?>" data-minute="<?php echo date("i",strtotime($end_time));?>"   ></div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="id" id="hiddenField3" value="{{$question->id}}" />
      <input type="hidden" name="q_id" id="hiddenField3" value="{{$question->q_id}}" />
      <input type="hidden" name="answer_type" id="answer_type" value="Text" />
      <input type="hidden" name="answer_text" id="answer_text" value="" />
      
      </td>
  </tr>
  <tr>
    <td align="center" valign="middle"><button type="submit" id="submit_butt" class="btn green" style="float:none !important;  font-size:20px; padding:10px 20px; ">التالي</button>
    <a style="display:none;" href="javascript: taps.loadajaxpage('/admin/endTest/{{$question->test_id}}')" ><div  class="btn red" style="float:none !important;  font-size:20px; padding:10px 20px; ">التالي</div></a>
    </td>
  </tr>
</table>

							      
										
								</div>
							  </div>
						 
						</div>
					</div>