<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/assign_pera" method="post" class="j-forms" id="j-forms" dir="rtl" novalidate>

  <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}" />
		{!! csrf_field() !!}

			<div class="content">

				<!-- start name --><!-- end name -->
@if(!isset($pera_pera->pera_email))
				<!-- start email -->
				<div class="j-row">
				<div class="unit">
                
					<label class="input select">
						<select name="pera_email" id="pera_email" data-placeholder="Select an Email"   >
							<option value="">Select an Email</option>
                            @foreach($pera_emails as $email)
							<option   value="{{$email->pera_email}}">{{$email->pera_email}}</option>
                            @endforeach
						</select>
					</label>
				</div>
            </div>
				<!-- end email -->
@endif
				<!-- start password -->
			  
				<!-- end password -->
				<!-- start Mobile --><!-- end Mobile -->
				<!-- start City --><!-- end City -->

				<!-- start response from server -->
			  <div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >تحديث</button>
			</div>
			<!-- end /.footer -->

		</form>
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					