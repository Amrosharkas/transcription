
<form class="form-horizontal ajaxForm" role="form" method="post" action="/admin/updateAnswer">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center">الوقت المتبقي</td>
        </tr>
        <tr>
            <td align="center">  
                <?php
                $end_time = date("Y-m-d H:i:s", strtotime($question->getOriginalTest->start_time . ' +60 seconds'));

                date_default_timezone_set("Africa/Cairo");
                $current_time = strtotime(date("Y-m-d H:i:s"));
                if ($current_time > strtotime($end_time)) {
                    $secounds_remaining = 1;
                } else {
                    $secounds_remaining = strtotime($end_time) - $current_time;
                }
                ?>

                <div class="timer" style="direction:ltr;" data-seconds-left="<?php echo $secounds_remaining;?>"></div>
            </td>
        </tr>
    </table>
    <div id="ajax_form1" style="margin-top:20px;" >
        @include('admin/question_form');
    </div>

</form>
