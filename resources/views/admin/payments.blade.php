<h2>{{$user->name}} Balance : {{$balance}}</h2>
@if($payments)
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        Your Payments
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
		


        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
        </div>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr class="tr-head">
                        <th class="table-checkbox">
                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Payment
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $payment)
                    <tr class="odd gradeX" id="data-row-{{$payment->id}}">
                        <td valign="middle">

                            <input type="checkbox" name="users[]"  class="checkboxes"  value="{{$payment->id}}" >
                        </td>
                        <td valign="middle">
                            {{$payment->datetime}}
                        </td>
                        <td valign="middle">
                            {{$payment->payment}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endif
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        Your Work
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
		


        </div>
    </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      </div>
          <table class="table table-striped table-bordered table-hover" id="sample_1">
              <thead>
                  <tr class="tr-head">
                      <th class="table-checkbox">
                          <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
                      </th>
                      <th>
                          From
                      </th>
                      <th>To</th>
                      <th>
                          Project
                      </th>
                        <th>Number of Utterances</th>
                        <th>Payment</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($utts as $utt)
                  <tr class="odd gradeX" id="data-row-{{$utt->id}}">
                      <td valign="middle">

                          <input type="checkbox" name="users[]"  class="checkboxes"  value="{{$utt->id}}" >
                      </td>
                      <td valign="middle">
                      <?php if(
                      $utt->date_from ==NULL){?>
                      Start of project
                      <?php
					  }else{?>
                     <?php echo date("d-m-Y",strtotime($utt->date_from));?>
                     </br>
                     <span dir="ltr"><?php echo date("h:i A",strtotime($utt->date_from));?></span>
                      <?php ;}?>
                          
                      </td>
                      <td valign="middle">
                      <?php echo date("d-m-Y",strtotime($utt->created_at));?>
                     </br>
                     <span dir="ltr"><?php echo date("h:i A",strtotime($utt->created_at));?></span>
                      </td>
                      <td valign="middle">
                          {{$utt->project}}
                      </td>
                        <td valign="middle">{{$utt->diff_ammount}}</td>
                        <td valign="middle">{{$utt->payment}}</td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
    </div>
</div>
