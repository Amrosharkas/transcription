<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/user/save" method="post" class="j-forms" id="j-forms" dir="rtl" novalidate>

 @if(isset($user)) <input type="hidden" name="id" id="id" value="{{$user->id}}" />@endif
		{!! csrf_field() !!}

			<div class="content">

				<!-- start name -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="name">
								<i class="fa fa-user"></i>
							</label>
							<input type="text" id="name" name="name" @if(isset($user)) value="{{$user->name}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">الاسم</label>
					</div>
				</div>
				<!-- end name -->

				<!-- start email -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="email" >
								<i class="fa fa-envelope-o"></i>
							</label>
                          
							<input type="email" id="email" name="email" disabled="disabled" @if(isset($user))value="{{$user->email}}"@endif>
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">البريد الالكتروني</label>
					</div>
				</div>
				<!-- end email -->

				<!-- start password -->
			  <div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="password">
								<i class="fa fa-lock"></i>
							</label>
							<input type="password" id="password" name="password">
					      
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">كلمة المرور</label>
					</div>
				</div>
				<!-- end password -->
				<!-- start Mobile -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="mobile">
								<i class="fa fa-phone"></i>
							</label>
							<input type="text" id="mobile" name="mobile" @if(isset($user)) value="{{$user->mobile}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">المحمول</label>
					</div>
				</div>
				<!-- end Mobile -->
				<!-- start City -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="city">
								<i class="fa fa-map-marker"></i>
							</label>
							<input type="text" id="city" name="city" @if(isset($user)) value="{{$user->city}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">المدينة</label>
					</div>
				</div>
				<!-- end City -->
                @if($user->pera_email)
				<!-- start Pera_email -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="Pera_email">
								<i class="fa fa-envelope-o"></i>
							</label>
							<input disabled="disabled" type="text" id="Pera_email" name="Pera_email" @if(isset($user)) value="{{$user->pera_email}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">Pera Pera Email</label>
					</div>
				</div>
				<!-- end Pera_email -->
				<!-- start Pera_password -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="Pera_password">
								<i class="fa fa-user"></i>
							</label>
							<input disabled="disabled" type="text" id="Pera_password" name="Pera_password" @if(isset($user)) value="{{$user->pera_password}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">Pera Pera Password</label>
					</div>
				</div>
				<!-- end Pera_password -->
				@endif
				<!-- start response from server -->
			  <div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >تحديث</button>
			</div>
			<!-- end /.footer -->

		</form>
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Success</h4>
										</div>
										<div class="modal-body">
											 Successfully registered
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>