<style>
#sample_1_length,#sample_1_filter{
	display:none;
}
</style>
<div class="portlet box grey-cascade">
	<div class="portlet-title">
		<div class="caption">
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			
			
			
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6"></div>
			</div>
		</div>
        <form name="vpb_form_name" id="deleteMult" method="post" action="">
        {!! csrf_field() !!}
		<table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		<tr class="tr-head">
			<th class="table-checkbox" style="display:none;">
				<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
			</th>
			<th>
				&nbsp;&nbsp;الملف&nbsp;&nbsp;
			</th>
			<th>
				 الاجابة النموذجية
			</th>
		</tr>
		</thead>
		<tbody>
        @foreach($answers as $answer)
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle"><audio preload="auto" controls >
				<source src="/sessions/examples/Audio ({{$answer->ID}}).WAV" >
			</audio></td>
			<td valign="middle">{{$answer->asnwer}}
                                </td>
		</tr>
        @endforeach
		</tbody>
		</table>
        </form>
	</div>
</div>