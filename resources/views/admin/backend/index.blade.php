<?php $i=3; $j =2;?>
@extends('admin.master')
@section('add_css')
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
   
@stop

@section('add_js_plugins')
	<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/table-managed.js"></script>
	<script type="text/javascript">
		/***************************************/
		/* After ajax is loaded  */
		/***************************************/
		taps.onajaxpageload=function(pageurl){
			
			if(pageurl.includes("Dismissed")){
				$(".sub-menu .active").removeClass("active");
				$(".dismissed").addClass("active");
			}
			if(pageurl.includes("Accepted")){
				$(".sub-menu .active").removeClass("active");
				$(".accepted").addClass("active");
			}
			if(pageurl.includes("Rejected")){
				$(".sub-menu .active").removeClass("active");
				$(".rejected").addClass("active");
			}
			
	$(document).ready(function(){
		
		$("#button12").click(function(){
			
			var user_id = $("#userr_id").val();
			 $(".log-input").each(function(){
				 project_id = $(this).attr('data-project-id');
				 
				 var input_value = $(this).val();
				 var $field = $(this);
				 $.ajax({url: "/admin/backend/project/checkLastValue/"+user_id+"/"+project_id+"", success: function(result){
					
				
					if( parseInt(input_value) < parseInt(result) ||  input_value == ""){
						$("#addLog").attr('data-submit',0) ;
						$field.css("background-color","#e7505a");
						$field.attr("data-valid",0);
					}else{
						$field.css("background-color","white");	
						$field.attr("data-valid",1);
					}
				
				
				}});
				
				
			});
			
			setTimeout(function(){
				invalidInputs = $("#addLog").find("[data-valid='0']").length;
				
				if(invalidInputs == 0){
					
					$("#addLog").submit() 
				}
					
				
				
			}, 1000);
			
				
		});
	});
			/***************************************/
			/* Color Picker */
			/***************************************/
			$(function() {
				// HEX
				$(".hex").spectrum({
					
					preferredFormat: "hex",
					showInput: true
				});
				// Method "show"
				$("#hex, #hsl, #rgb, #a-hsl, #a-rgb, #palette1, #palette2").show();
			});

			/***************************************/
			/* Checkboxes handling */
			/***************************************/
                var checkboxes = $("input[type='checkbox']"),
    		submitButt = $(".multiple-controls");
			checkboxes.click(function() {
				submitButt.attr("disabled", !checkboxes.is(":checked"));
			});
			
			/***************************************/
			/* Show back button */
			/***************************************/
			$("#back-btn").show();
			$("#back-btn").attr("href","javascript: taps.loadajaxpage('/admin/backend/getUsers');") ;
			
			$(document).ready(function(){

});
			/***************************************/
			/* Inistantiate data tables */
			/***************************************/
			TableManaged.init();
			/***************************************/
			/* Inistantiate Modal plugin to confirm deleting */
			/***************************************/
			UIAlertDialogApi.init();

		}
                $(document).on('change','.update-checked',function (){
                    postData = {
                        '_token': '{{csrf_token()}}',
                        'user_id': $(this).data('user'),
                        'checked' : $(this).is(":checked") *1
                    }
                    $.post('/admin/backend/update-checked',postData);
                })
                $(document).on('click','.accept_multiple',function(){
                    bootbox.confirm("Are you sure you want to accept multiple users ?", function(result) {
                       if(result == true){
                        $("#deleteMult").ajaxSubmit({
                              url: "/admin/backend/accept-multiple", 
                              type: 'post',
                              success: acceptedSuccess,
                              error: errorFn
                        });

                        function errorFn(xhr, status, strErr){

                           msg = "All users were not accepted"	;
                                                    title = "There was an error";
                                                    theme ="error";
                                            var $toast = toastr[theme](title, msg);
                        }
                        function acceptedSuccess(data){

                            for(var i = 0; i<data.length; i++){
                                //Bad idea to use .each(), much slower in this case
                                var table = $('#sample_1').DataTable();

                                table
                                    .row( $("#data-row-"+data[i]) )
                                    .remove()
                                    .draw();

                                //$("#data-row-"+arr[i]).remove();

                            }
                          //$("#data-row-"+stuffId).remove();
                           msg = "Successfully Accepted"	;
                                                    title = "";
                                                    theme ="info";
                                            var $toast = toastr[theme](title, msg);  
                        }



                       }
                    }); 
            });
                $(document).on('click','.reject_multiple',function(){
                    bootbox.confirm("Are you sure you want to reject multiple users ?", function(result) {
                       if(result == true){
                        $("#deleteMult").ajaxSubmit({
                              url: "/admin/backend/reject-multiple", 
                              type: 'post',
                              success: rejectedSuccess,
                              error: errorFn
                        });

                        function errorFn(xhr, status, strErr){

                           msg = "All users were not rejected"	;
                                                    title = "There was an error";
                                                    theme ="error";
                                            var $toast = toastr[theme](title, msg);
                        }
                        function rejectedSuccess(data){

                            for(var i = 0; i<data.length; i++){
                                //Bad idea to use .each(), much slower in this case
                                var table = $('#sample_1').DataTable();

                                table
                                    .row( $("#data-row-"+data[i]) )
                                    .remove()
                                    .draw();

                                //$("#data-row-"+arr[i]).remove();

                            }
                          //$("#data-row-"+stuffId).remove();
                           msg = "Successfully Rejected"	;
                                                    title = "";
                                                    theme ="info";
                                            var $toast = toastr[theme](title, msg);  
                        }



                       }
                    }); 
            });

	</script>
@stop
@section('add_inits')
	
	
@stop
@section('title')
	{{$current_user->name}}
@stop

@section('page_title')
	{{$current_user->name}}
@stop

@section('page_title_small')
	
@stop

@section('content')

<div id="countrytabs" class="shadetabs" style="display:none;" >
    <li><a href="/admin/backend/{{$filterRoute}}" rel="countrycontainer">All users</a></li>
</div>

@stop



