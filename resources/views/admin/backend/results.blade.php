@extends('admin.master')
<?php
$i = 1;
$j = 2;
?>
@section('add_css')
<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<style>
    #ajaxBody{
        overflow: auto;
    }
</style>
@stop

@section('add_js_plugins')
<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

@stop

@section('add_js_scripts')
<script src="/assets/admin/pages/scripts/table-managed.js"></script>

<script>
    $('.update-correct').change(function () {
        postData = {
            _token: "{{csrf_token()}}",
            id: $(this).data('ans'),
            val: $(this).val(),
        }
        $.post('/admin/backend/update-correct', postData)
    })
    TableManaged.init();
</script>
@stop
@section('add_inits')


@stop
@section('title')
{{$user_name}}
@stop

@section('page_title')
{{$user_name}}
@stop

@section('page_title_small')

@stop

@section('content')
<table class="table table-striped table-bordered table-hover" id="sample_dsa">
    <tr>
        <th>Audio</th>
        <th>Answer type</th>
        <th>Answer text</th>
        <th>Right Answer type</th>
        <th>Right answer text</th>
        <th>Right answer text 2</th>
        <th>Correct </th>
    </tr>
    @foreach($answers as $ans)
    <tr>
        <td>
            ({{$ans->q_id}})
            <audio preload="auto" controls>
                <source src="/sessions/test/Audio ({{$ans->audio_number}}).WAV" >
            </audio>
        </td>
        <td>
            {{$ans->answer_type}}
        </td>
        <td>
            {{$ans->answer_text}}
        </td>
        <td>
            {{$ans->right_type}}
        </td>
        <td>
            {{$ans->right_text1}}
        </td>
        <td>
            {{str_replace('1231456465413216546516549846546846843146846541','',$ans->right_text2)}}
        </td>
        <td>
            <select class="update-correct " data-ans="{{$ans->id}}">
                <option @if($ans->correct == 0) selected @endif>0</option>
                <option @if($ans->correct == 1) selected @endif>1</option>
                <option @if($ans->correct == 2) selected @endif>2</option>
            </select>
        </td>
    </tr>
    @endforeach
</table>
@stop



