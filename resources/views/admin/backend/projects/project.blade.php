<?php $i=4; $j=1;?>
@extends('admin.master')
@section('add_css')
@stop

@section('add_js_plugins')
@stop

@section('add_js_scripts')
<script type="text/javascript">
	
	$(document).ready(function(){
		
		$("#button12").click(function(){
			
			
			var project_id = $("#prroject_id").val();
			 $(".log-input").each(function(){
				 user_id = $(this).attr('data-user-id');
				 var input_value = $(this).val();
				 var $field = $(this);
				 $.ajax({url: "checkLastValue/"+user_id+"/"+project_id+"", success: function(result){
					
				
					if( parseInt(input_value) < parseInt(result) ||  input_value == ""){
						$("#addLog").attr('data-submit',0) ;
						$field.css("background-color","#e7505a");
						$field.attr("data-valid",0);
					}else{
						$field.css("background-color","white");	
						$field.attr("data-valid",1);
					}
				
				
				}});
				
				
			});
			
			setTimeout(function(){
				invalidInputs = $("#addLog").find("[data-valid='0']").length;
				
				if(invalidInputs == 0){
					
					$("#addLog").submit() 
				}
					
				
				
			}, 1000);
			
				
		});
	});
</script>
@stop
@section('add_inits')


@stop
@section('title')
{{$project->title}}
@stop

@section('page_title')
{{$project->title}}
@stop

@section('page_title_small')

@stop

@section('content')
<div class="row">
  <form class="form-inline" action="/admin/backend/project/add-user" method="post">
        {!! csrf_field() !!}
    <input type="hidden" name="project" value="{{$project->id}}">
    <select class="form-control" name="user">
            @foreach($acceptedUsers as $user)
            <option value="{{$user->id}}" dir="ltr">{{$user->pera_email}} - ({{$user->name}}) </option>   
            @endforeach
        </select>
    <button class="btn btn-primary" type="submit">Add to project</button>
  </form>
</div>
<div class="row">&nbsp;</div>
<div class="row">
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        
            Add new Log
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
		


        </div>
    </div>
    <div class="portlet-body">
      <form action="/admin/backend/log/submit" method="post" id="addLog" data-submit ="1">
{!! csrf_field() !!}
<input name="project_id" value="{{$project->id}}" type="hidden" id="prroject_id"  />
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="sample_1" class="table table-striped table-bordered table-hover">
  <tr>
    <th align="right" scope="col">Name</th>
    <th align="right" scope="col">Pera Pera</th>
    <th align="right" scope="col">&nbsp;</th>
    <th align="right" scope="col">Actions</th>
    </tr>
  
 @foreach($project->getProjectUsers as $user) 
 <tr>
    <td align="right">{{$user->name}}</td>
    <td align="right">{{$user->pera_email}}</td>
    <td align="right">
      <input class="log-input" type="text" name="log-{{$user->id}}" data-user-id = "{{$user->id}}" data-valid ="" id="textfield" />
    </td>
    <td align="right"><a href="/admin/backend/remove/{{$user->id}}/{{$project->id}}" > <button type="button"  class="btn red delete"><i class="fa fa-remove"></i> Remove</button> </a></td>
    </tr>
  @endforeach
 <tr>
    <td align="right"></td>
    <td align="right"></td>
    <td align="right"><a  class="btn btn-default"  id="button12"  >Submit new log</a>
    </td>
    <td align="right">&nbsp;</td>
    </tr>
</table>
</form>
    </div>
</div>




</div>


</br>
<div class="row">
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        Log History
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
		


        </div>
    </div>
    <div class="portlet-body">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered table-hover">
  <tr>
    <th align="right" scope="col">Log Date</th>
    <th align="right" scope="col">Last modified by</th>
    </tr>
  
 @foreach($logs as $log) 
 <tr>
   <td align="right"><a href="/admin/backend/log/{{$log->id}}">{{$log->datetime}}</a></td>
   <td align="right">&nbsp;</td>
    </tr>
  @endforeach
 <tr>
   <td align="right"></td>
   <td align="right"></td>
    </tr>
</table>
    </div>
</div>

</div>


@stop
