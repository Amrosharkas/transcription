<?php $i=4; $j=1;?>
@extends('admin.master')
@section('add_css')
@stop

@section('add_js_plugins')
@stop

@section('add_js_scripts')
<script type="text/javascript">
	
	$(document).ready(function(){
		
		$("#button12").click(function(){
			
			
			var project_id = $("#prroject_id").val();
			 $(".log-input").each(function(){
				 user_id = $(this).attr('data-user-id');
				 
				 var input_value = $(this).val();
				 var $field = $(this);
				 $.ajax({url: "../project/checkLastValue/"+user_id+"/"+project_id+"", success: function(result){
					
				
					if( parseInt(input_value) < parseInt(result) ||  input_value == ""){
						$("#addLog").attr('data-submit',0) ;
						$field.css("background-color","#e7505a");
						$field.attr("data-valid",0);
					}else{
						$field.css("background-color","white");	
						$field.attr("data-valid",1);
					}
				
				
				}});
				
				
			});
			
			setTimeout(function(){
				invalidInputs = $("#addLog").find("[data-valid='0']").length;
				
				if(invalidInputs == 0){
					
					$("#addLog").submit() 
				}
					
				
				
			}, 1000);
			
				
		});
	});
</script>
@stop
@section('add_inits')


@stop
@section('title')
@stop

@section('page_title')
@stop

@section('page_title_small')

@stop

@section('content')

<div class="row">
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        
            Update Log
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
		


        </div>
    </div>
    <div class="portlet-body">
      <form action="/admin/backend/log/update" method="post" id="addLog" data-submit ="1">
{!! csrf_field() !!}
<input name="project_id" value="{{$project->id}}" type="hidden" id="prroject_id" />
<input name="log_id" value="{{$log->id}}" type="hidden"  />

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered table-hover">
  <tr>
    <th align="right" scope="col">Name</th>
    <th align="right" scope="col">Pera Pera</th>
    <th align="right" scope="col">&nbsp;</th>
    </tr>
  
 @foreach($log_details as $log_detail) 
 <tr>
    <td align="right">{{$log_detail->getUser->name}}</td>
    <td align="right">{{$log_detail->getUser->pera_email}}</td>
    <td align="right"><span id="sprytextfield1">
      <input class="log-input"   type="text" name="log-{{$log_detail->user_id}}" data-user-id = "{{$log_detail->user_id}}" data-valid ="" id="textfield" value="{{$log_detail->ammount}}"  @if($check_last_log >0) disabled="disabled" @endif/>
      </span></td>
    </tr>
  @endforeach
 <tr>
    <td align="right"></td>
    <td align="right"></td>
    <td align="right"><a  class="btn btn-default"  id="button12"  >Update log</a>
    </td>
    </tr>
</table>
</form>
    </div>
</div>




</div>
@stop
