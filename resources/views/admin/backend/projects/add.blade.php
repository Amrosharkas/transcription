<?php $i =4; $j =1;?>
@extends('admin.master')
@section('add_css')
@stop

@section('add_js_plugins')
@stop

@section('add_js_scripts')
@stop
@section('add_inits')


@stop
@section('title')
Projects
@stop

@section('page_title')
New Project
@stop

@section('page_title_small')

@stop

@section('content')
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
        Log History
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
		


        </div>
    </div>
    <div class="portlet-body">
      <form class="form" method="post" action="/admin/backend/projects/save">
    {!! csrf_field() !!}
    <div class="form-group">
        <label>Title</label>
        <input type="text" class="form-control" name="title">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Add</button>
    </div>
</form>
    </div>
</div>

@stop





