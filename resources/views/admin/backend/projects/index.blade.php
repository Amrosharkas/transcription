<?php 
if($status == "active"){
	$i =4; $j=1;
}else{
	$i =4; $j=3;
}

?>
@extends('admin.master')
@section('add_css')
<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('add_js_plugins')
<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
@stop

@section('add_js_scripts')
<script>
    $(document).ready(function () {
        $('#projects-table').DataTable();
    });
</script>
@stop
@section('add_inits')


@stop
@section('title')
Projects
@stop

@section('page_title')
Projects
@stop

@section('page_title_small')

@stop

@section('content')
<a class="btn btn-primary" href="/admin/backend/projects/add">Add new</a>
<div class="row">&nbsp;</div>
<table class="table table-bordered" id="projects-table">
    <thead>
        <tr>
            <th>Title</th>
            <th>Created at</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($projects as $project)
        <tr id="project-{{$project->id}}}}">
            <td>
                {{$project->title}}
            </td>
            <td>
                {{$project->created_at}}
            </td>
            <td>
            <a href="/admin/backend/project/{{$project->id}}">
              <button type="button" class="btn green"><i class="fa fa-edit"></i> Edit</button>
            </a>
            <a href="/admin/backend/project/change_status/{{$project->id}}">
              <button type="button" class="btn green"><i class="fa fa-edit"></i> 
              @if($project->project_active == 0)Re-activate  @endif
             @if($project->project_active == 1) Complete @endif
              </button>
            </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop




