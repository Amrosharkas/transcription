<div class="page-footer">
	<div class="page-footer-inner">
		 2016 &copy; Arab Localizer Transcription management system by . <a href="http://cloudwrld.com" title="Cloud World Official Website" target="_blank">Cloud World</a>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-82093272-1', 'auto');
  ga('send', 'pageview');

</script>