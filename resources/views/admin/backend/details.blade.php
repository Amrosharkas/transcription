@extends('admin.master')
<?php $i=1; $j =2;?>
@section('add_css')
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('add_js_plugins')
	<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/table-managed.js"></script>
	<script type="text/javascript">
		/***************************************/
		/* After ajax is loaded  */
		/***************************************/
		taps.onajaxpageload=function(pageurl){

			/***************************************/
			/* Color Picker */
			/***************************************/
			$(function() {
				// HEX
				$(".hex").spectrum({
					
					preferredFormat: "hex",
					showInput: true
				});
				// Method "show"
				$("#hex, #hsl, #rgb, #a-hsl, #a-rgb, #palette1, #palette2").show();
			});

			/***************************************/
			/* Checkboxes handling */
			/***************************************/
			var checkboxes = $("input[type='checkbox']"),
    		submitButt = $(".delete_multiple");
			checkboxes.click(function() {
				submitButt.attr("disabled", !checkboxes.is(":checked"));
			});
			
			/***************************************/
			/* Show back button */
			/***************************************/
			$("#back-btn").show();
			$("#back-btn").attr("href","javascript: taps.loadajaxpage('/admin/getUsers');") ;
			
			$(document).ready(function(){

	

				/***************************************/
				/* Form validation */
				/***************************************/
				$( '#j-forms' ).validate({
			
					/* @validation states + elements */
					errorClass: 'error-view',
					validClass: 'success-view',
					errorElement: 'span',
					onkeyup: false,
					onclick: false,
			
					/* @validation rules */
					rules: {
						name: {
							required: true
						},
						email: {
							required: true,
							email: true
						},
						login: {
							required: true
						},
						password: {
							required: true,
							minlength: 6
						}
					},
					messages: {
						name: {
							required: 'Please enter your name'
						},
						email: {
							required: 'Please enter your email',
							email: 'Incorrect email format'
						},
						login: {
							required: 'Please enter your login'
						},
						password: {
							required: 'Please enter your password',
							minlength: 'At least 6 characters'
						}
					},
					// Add class 'error-view'
					highlight: function(element, errorClass, validClass) {
						$(element).closest('.input').removeClass(validClass).addClass(errorClass);
						if ( $(element).is(':checkbox') || $(element).is(':radio') ) {
							$(element).closest('.check').removeClass(validClass).addClass(errorClass);
						}
					},
					// Add class 'success-view'
					unhighlight: function(element, errorClass, validClass) {
						$(element).closest('.input').removeClass(errorClass).addClass(validClass);
						if ( $(element).is(':checkbox') || $(element).is(':radio') ) {
							$(element).closest('.check').removeClass(errorClass).addClass(validClass);
						}
					},
					// Error placement
					errorPlacement: function(error, element) {
						if ( $(element).is(':checkbox') || $(element).is(':radio') ) {
							$(element).closest('.check').append(error);
						} else {
							$(element).closest('.unit').append(error);
						}
					},
					// Submit the form
					submitHandler:function() {
						$( '#j-forms' ).ajaxSubmit({
			
							// Server response placement
							target:'#j-forms #response',
							
							// If error occurs
							error:function(xhr) {
								//$('#j-forms #response').html('An error occured: ' + xhr.status + ' - ' + xhr.statusText);
								msg = "There was an error"	;
									title = "Failed";	
									theme ="error";
								var $toast = toastr[theme](title, msg);
								$('#j-forms button[type="submit"]').attr('disabled', false).removeClass('processing');
							},
			
							// Before submiting the form
							beforeSubmit:function(){
								// Add class 'processing' to the submit button
								$('#j-forms button[type="submit"]').attr('disabled', true).addClass('processing');
							},
			
							// If success occurs
							success:function(xhr){
								//alert(xhr);
									msg = "Successfully Saved"	;
									title = "Congratulations";
									theme ="success";
								var $toast = toastr[theme](title, msg); // Wire up an event handler to a button in the toast, if it exists
			
								// Remove class 'processing'
								$('#j-forms button[type="submit"]').attr('disabled', false).removeClass('processing');
			
								// Remove classes 'error-view' and 'success-view'
								$('#j-forms .input').removeClass('success-view error-view');
								$('#j-forms .check').removeClass('success-view error-view');
			
								// If response from the server is a 'success-message'
								if ( $('#j-forms .success-message').length ) {
			
									// Reset form
									$('#j-forms').resetForm();
			
									// Make checkbox 'terms of use' unavailable
									$('#check-enable-button').attr('checked', false);
									$('#check-enable-button').attr('disabled', true);
			
									// Make 'submit' button unavailable while checkbox doesn't checked
									$('#enable-button').attr('disabled', true).addClass('disabled-view');
			
									setTimeout(function(){
										// Delete success message after 5 seconds
										$('#j-forms #response').removeClass('success-message').html('');
			
										// Make checkbox 'terms of use' available
										$('#check-enable-button').attr('disabled', false);
									}, 5000);
								}
							}
						});
					}
				});
				/***************************************/
				/* end form validation */
				/***************************************/
});
			/***************************************/
			/* Inistantiate data tables */
			/***************************************/
			TableManaged.init();
			/***************************************/
			/* Inistantiate Modal plugin to confirm deleting */
			/***************************************/
			UIAlertDialogApi.init();

		}
	</script>
@stop
@section('add_inits')
	
	
@stop
@section('title')
	{{$current_user->name}}
@stop

@section('page_title')
	{{$current_user->name}}
@stop

@section('page_title_small')
	
@stop

@section('content')
<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i></div>
							<div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
							
							</div>
						</div>
						<div class="portlet-body form">
							
							

		<form action="/admin/user/save" method="post" class="j-forms" id="j-forms" dir="rtl" novalidate>

 @if(isset($user)) <input type="hidden" name="id" id="id" value="{{$user->id}}" />@endif
		{!! csrf_field() !!}

			<div class="content">

				<!-- start name -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="name">
								<i class="fa fa-user"></i>
							</label>
							<input type="text" id="name" name="name" @if(isset($user)) value="{{$user->name}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">الاسم</label>
					</div>
				</div>
				<!-- end name -->

				<!-- start email -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="email" >
								<i class="fa fa-envelope-o"></i>
							</label>
                          
							<input type="email" id="email" name="email" disabled="disabled" @if(isset($user))value="{{$user->email}}"@endif>
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">البريد الالكتروني</label>
					</div>
				</div>
				<!-- end email -->

				<!-- start password -->
			  <div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="password">
								<i class="fa fa-lock"></i>
							</label>
							<input type="password" id="password" name="password">
					      
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">كلمة المرور</label>
					</div>
				</div>
				<!-- end password -->
				<!-- start Mobile -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="mobile">
								<i class="fa fa-phone"></i>
							</label>
							<input type="text" id="mobile" name="mobile" @if(isset($user)) value="{{$user->mobile}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">المحمول</label>
					</div>
				</div>
				<!-- end Mobile -->
				<!-- start City -->
				<div class="j-row">
					
					<div class="span8 unit">
						<div class="input">
							<label class="icon-right" for="city">
								<i class="fa fa-marker"></i>
							</label>
							<input type="text" id="city" name="city" @if(isset($user)) value="{{$user->city}}@endif" >
						</div>
					</div>
                    <div class="span4">
						<label class="label label-center">المدينة</label>
					</div>
				</div>
				<!-- end City -->

				<!-- start response from server -->
			  <div id="response" style="display:none;"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button" >تحديث</button>
			</div>
			<!-- end /.footer -->

		</form>
	
						</div>
						<!-- END VALIDATION STATES-->
					</div>
					<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Success</h4>
										</div>
										<div class="modal-body">
											 Successfully registered
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
@stop



