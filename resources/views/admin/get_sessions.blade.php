<style>
#sample_1_length,#sample_1_filter{
	display:none;
}
</style>
<div class="portlet box grey-cascade">
	<div class="portlet-title">
		<div class="caption">
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			
			
			
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6"></div>
			</div>
		</div>
        <form name="vpb_form_name" id="deleteMult" method="post" action="">
        {!! csrf_field() !!}
		<table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		<tr class="tr-head">
			<th class="table-checkbox" style="display:none;">
				<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" name="stuff-select[]"/>
			</th>
			<th>
				&nbsp;&nbsp; المادة&nbsp;&nbsp;
			</th>
			<th>
				 عرض
			</th>
		</tr>
		</thead>
		<tbody>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				1 - كيفية الدخول إلى حسابك
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/1')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
		  <td valign="middle" style="display:none;"></td>
		  <td valign="middle">1* - الدخول على البرنامج وبدء العمل</td>
		  <td valign="middle"><a href="/sessions/1.pdf">
		    <button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button>
		    </a></td>
		  </tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				2 - بدء العمل
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/2')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				3 - الحالات العادية
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/3')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				4 - حالات التخطي
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/4')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				5 - حالة تداخل الأصوات
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/5')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				 6 - حالة كتابة الموسيقى
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/6')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				 7 - حالات عدم فهم الكلام رغم استنتاج حروفه
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/7')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				 8 - التصنيف
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/8')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
		  <td valign="middle" style="display:none;"></td>
		  <td valign="middle">8* - كتابة التسجيلات بالحالات المختلفة</td>
		  <td valign="middle"><a href="/sessions/2.pdf">
		    <button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button>
		    </a></td>
		  </tr>
		<tr class="odd gradeX" >
			<td valign="middle" style="display:none;">
				
                
			</td>
			<td valign="middle">
				9 - حالة تسليم العمل
		  </td>
			<td valign="middle"><a href="javascript: taps.loadajaxpage('sessions/9')"><button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button></a>
                                </td>
		</tr>
		<tr class="odd gradeX" >
		  <td valign="middle" style="display:none;"></td>
		  <td valign="middle">10 - القواعد العامة للكتابة</td>
		  <td valign="middle"><a href="/sessions/3.pdf">
		    <button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button>
		    </a></td>
		  </tr>
		<tr class="odd gradeX" >
		  <td valign="middle" style="display:none;"></td>
		  <td valign="middle">11 - الأمثلة</td>
		  <td valign="middle"><a href="javascript: taps.loadajaxpage('getExamples')">
          <button type="button" class="btn green"><i class="fa fa-view"></i> عرض</button>
          </a></td>
		  </tr>
		</tbody>
		</table>
        </form>
	</div>
</div>