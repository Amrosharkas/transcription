@extends('admin.master_login')

@section('add_css')
	<link href="/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
@stop

@section('add_js_plugins')
	<script src="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
@stop

@section('add_inits')
	Login.init();
@stop

@section('title')
	Login
@stop

@section('logo')

<div class="logo">
	<table width="200" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
			    <td align="left" valign="middle"><img src="/assets/admin/layout/img/logo-invert.png" alt="logo"  class="logo-default" style="max-height:40px;"/></td>
			    <td align="left" valign="middle"><div ><a href="#"><strong > Cloud Management</strong></a></div></td>
		      </tr>
		  </table>
</div>

@stop

@section('login_form')

<form class="login-form" action="/auth/login" method="post">
	{!! csrf_field() !!}
		<h3 class="form-title">Login</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Please enter your email and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" value="{{ old('email') }}" />
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Login</button>
			<label class="rememberme check">
			<input type="checkbox" name="remember" />Remember </label>
			<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
		</div>
	
		
		<div class="create-account">
			
		</div>
	</form>
    
    

@stop
@section('forget_form')

<form class="forget-form" role="form" method="POST" action="{{ url('/password/email') }}">
{!! csrf_field() !!}
		<h3>Forget Password ?</h3>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
            <input type="email" class="form-control placeholder-no-fix" name="email" value="{{ old('email') }}">
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn btn-default">Back</button>
			<button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
		</div>
	</form>

@stop