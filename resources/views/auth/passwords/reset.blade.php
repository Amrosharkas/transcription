@extends('admin.master_login')

@section('add_css')
	<link href="/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
@stop

@section('add_js_plugins')
	<script src="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
@stop

@section('add_js_scripts')
	<script src="/assets/admin/pages/scripts/login.js" type="text/javascript"></script>
@stop

@section('add_inits')
	Login.init();
@stop

@section('title')
	Login
@stop

@section('logo')

<div class="logo">
	<a href="index.html">
	<img src="/assets/admin/layout/img/logo-big.png" alt=""/>
	</a>
</div>

@stop

@section('login_form')

<form class="login-form" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}
<input type="hidden" name="token" value="{{ $token }}">
		<h3 class="form-title">Reset Password</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<input type="email" class="form-control" name="email" value="{{ $email or old('email') }}">
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input type="password" class="form-control" name="password">
		</div>
        <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Reset Password</label>
			<input type="password" class="form-control" name="password_confirmation">
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Reset Password</button>
			<label class="rememberme check">
			
		</div>
		
		<div class="create-account">
			<p>
				
			</p>
		</div>
	</form>

@stop
@section('forget_form')

@stop
