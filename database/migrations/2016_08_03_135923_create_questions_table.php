<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('audio_number');
            $table->string('right_type');
            $table->string('right_text1');
            $table->string('right_text2');
            $table->timestamps();
        });
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->datetime('start_time');
            $table->string('status');
            $table->timestamps();
        });
        Schema::create('test_asnwers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('test_id');
            $table->integer('q_id');
            $table->string('answer_type');
            $table->string('answer_text');
            $table->integer('correct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
