<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Crate)logsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('datetime');
            $table->integer('added_by');
            $table->integer('project_id');
            $table->timestamps();
        });
        Schema::create('log_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('log_id');
            $table->integer('project_id');
            $table->integer('added_by');
            $table->integer('user_id');
            $table->integer('ammount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
