var UIAlertDialogApi = function () {

    var handleDialogs = function() {

      
            //Single Delete Handler
            $('.delete').click(function(){
                stuffId = $(this).attr('id');
                modelName = $(this).attr('data-model');
                bootbox.confirm("Are you sure you want to delete ?", function(result) {
                   if(result == true){
                      
                    $.ajax({
                        type: "GET",
                        url: "/admin/delete/"+modelName+"/"+stuffId+"",
                        dataType: "html",
                        success: deleteSuccess,
                        error: errorFn
                        

                    });
                    function errorFn(xhr, status, strErr){
		             
                       msg = "Nothing Deleted"	;
						title = "There was an error";
						theme ="error";
					var $toast = toastr[theme](title, msg);
                    }
                    function deleteSuccess(result){
                        var table = $('#sample_1').DataTable();

                            table
                                .row( $("#data-row-"+stuffId) )
                                .remove()
                                .draw();
                      //$("#data-row-"+stuffId).remove();
                       msg = "Successfully Deleted"	;
						title = "";
						theme ="info";
					var $toast = toastr[theme](title, msg);  
                    }
                    
                       
                   }
                }); 
            });
            //Multiple Delete Handler
        $('.delete_multiple').click(function(){
                
                modelName = $(this).attr('data-model');
           
                bootbox.confirm("Are you sure you want to delete multiple items ?", function(result) {
                   if(result == true){
                    $("#deleteMult").ajaxSubmit({
                          url: "/admin/delete/multiple/"+modelName+"", 
                          type: 'post',
                          success: deleteSuccess,
                          error: errorFn
                    });
                   
                    function errorFn(xhr, status, strErr){
		             
                       msg = "Nothing Deleted"	;
						title = "There was an error";
						theme ="error";
					var $toast = toastr[theme](title, msg);
                    }
                    function deleteSuccess(data){
                        var arr = data.split(',');

                        for(var i = 0; i<arr.length; i++){
                            //Bad idea to use .each(), much slower in this case
                            var table = $('#sample_1').DataTable();

                            table
                                .row( $("#data-row-"+arr[i]) )
                                .remove()
                                .draw();
                            
                            //$("#data-row-"+arr[i]).remove();
                            
                        }
                      //$("#data-row-"+stuffId).remove();
                       msg = "Successfully Deleted"	;
						title = "";
						theme ="info";
					var $toast = toastr[theme](title, msg);  
                    }
                    
                    
                       
                   }
                }); 
            });

            
          

    }

    var handleAlerts = function() {
        
        $('#alert_show').click(function(){

            Metronic.alert({
                container: $('#alert_container').val(), // alerts parent container(by default placed after the page breadcrumbs)
                place: $('#alert_place').val(), // append or prepent in container 
                type: $('#alert_type').val(),  // alert's type
                message: $('#alert_message').val(),  // alert's message
                close: $('#alert_close').is(":checked"), // make alert closable
                reset: $('#alert_reset').is(":checked"), // close all previouse alerts first
                focus: $('#alert_focus').is(":checked"), // auto scroll to the alert after shown
                closeInSeconds: $('#alert_close_in_seconds').val(), // auto close after defined seconds
                icon: $('#alert_icon').val() // put icon before the message
            });

        });

    }

    return {

        //main function to initiate the module
        init: function () {
            handleDialogs();
            handleAlerts();
        }
    };

}();