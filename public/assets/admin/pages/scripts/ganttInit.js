var GanttInit = function () {
    // Adjust Gantt Height on init
    var h = window.innerHeight;
    h = h - 180;
    $(".col-md-12").css("height", h);
    // Adjust Gantt Height on screen resize
    $(window).resize(function () {
        h = window.innerHeight;
        h = h - 180;
        $(".col-md-12").css("height", h);
        $('span').text(x += 1);
    });

    var initCal1 = function init() {
        
       
        gantt.config.columns[0].template = function (obj) {
            return obj.text ;
        }

        // Weekend highlighting
        gantt.config.work_time = true;
        gantt.templates.task_cell_class = function (task, date) {
            if (!gantt.isWorkTime(date))
                return "week_end";
            return "";
        };
        gantt.templates.scale_cell_class = function (date) {
            if (date.getDay() == 5 || date.getDay() == 6) {
                return "weekend";
            }
        };
        gantt.templates.task_cell_class = function (item, date) {
            if (date.getDay() == 5 || date.getDay() == 6) {
                return "weekend"
            }
        };
        // Changing the class of the task depending on its priority
        gantt.templates.task_class = function (start, end, task) {
            console.log(task.color);
            switch (task.priority) {
                case "1":
                    return "high";
                    break;
                case "2":

                    return "medium";
                    break;
                case "3":
                    return "low";
                    break;
                case "4":
                    return "junior";
                    break;
            }

        };

        // Grid size
        gantt.config.grid_width = 350;
        gantt.config.date_grid = "%F %d";
        
        //Scales
        gantt.config.scale_unit = "year";
        gantt.config.step = 1;
        gantt.config.date_scale = "%Y";
        gantt.config.min_column_width = 15;
        gantt.config.scale_height = 60;
        
        gantt.config.subscales = [

            {
                unit: "month",
                step: 1,
                date: "%M"
            },
            {
                unit: "day",
                step: 1,
                date: "%d"
            }

	];

        // Text Positioning depending on free space
        (function () {
            gantt.config.font_width_ratio = 7;
            gantt.templates.leftside_text = function leftSideTextTemplate(start, end, task) {
                if (getTaskFitValue(task) === "left") {
                    return task.text;
                }
                return "";
            };
            gantt.templates.rightside_text = function rightSideTextTemplate(start, end, task) {
                if (getTaskFitValue(task) === "right") {
                    return task.text;
                }
                return "";
            };
            gantt.templates.task_text = function taskTextTemplate(start, end, task) {
                if (getTaskFitValue(task) === "center") {
                    return task.text;
                }
                return "";
            };

            function getTaskFitValue(task) {
                var taskStartPos = gantt.posFromDate(task.start_date),
                    taskEndPos = gantt.posFromDate(task.end_date);

                var width = taskEndPos - taskStartPos;
                var textWidth = (task.text || "").length * gantt.config.font_width_ratio;

                if (width < textWidth) {
                    var ganttLastDate = gantt.getState().max_date;
                    var ganttEndPos = gantt.posFromDate(ganttLastDate);
                    if (ganttEndPos - taskEndPos < textWidth) {
                        return "left"
                    } else {
                        return "right"
                    }
                } else {
                    return "center";
                }
            }
        })();
        gantt.templates.progress_text = function (start, end, task) {
            return "<span style='text-align:left;'>" + Math.round(task.progress * 100) + "% </span>";
        };

        gantt.templates.leftside_text = function (start, end, task) {
            return task.duration + " days";
        };
        // Fullscreen Handling
        gantt.attachEvent("onTemplatesReady", function () {
            var toggle = document.createElement("i");
            toggle.className = "fa fa-expand gantt-fullscreen";
            gantt.toggleIcon = toggle;
            gantt.$container.appendChild(toggle);
            toggle.onclick = function () {
                if (!gantt.getState().fullscreen) {
                    gantt.expand();
                } else {
                    gantt.collapse();
                }
            };
        });
        gantt.attachEvent("onExpand", function () {
            var icon = gantt.toggleIcon;
            if (icon) {
                icon.className = icon.className.replace("fa-expand", "fa-compress");
            }

        });
        gantt.attachEvent("onCollapse", function () {
            var icon = gantt.toggleIcon;
            if (icon) {
                icon.className = icon.className.replace("fa-compress", "fa-expand");
            }
        });



        // Today line
        var date_to_str = gantt.date.date_to_str(gantt.config.task_date);

        var today = new Date();
        gantt.addMarker({
            start_date: today,
            css: "today",
            text: "Today",
            title: "Today: " + date_to_str(today)
        });


        // Basic initialisation
        gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
        gantt.init("gantt_here");
        gantt.parse(tasks);

    }

    return {

        //main function to initiate the module
        init: function () {

            initCal1();

        }

    };

}();
