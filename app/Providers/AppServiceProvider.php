<?php

namespace App\Providers;
use Auth;
use App\Company;
use App\Company_holiday;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            date_default_timezone_set("Africa/Cairo");
            \View::composer('*', function($view){
             
            $view->with('current_user', \Auth::user());
            
             });
        
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
