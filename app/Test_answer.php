<?php

namespace App;
use App\Question;
use App\Test;
use Illuminate\Database\Eloquent\Model;

class Test_answer extends Model
{
	protected $fillable = ['id','user_id','test_id','q_id','answer_type','answer_text','correct'];
    protected $table = 'test_asnwers';

    public function getOriginalQuestion(){
    	Return $this->hasOne('App\Question', 'id', 'q_id');
    }
    public function getOriginalTest(){
    	Return $this->hasOne('App\Test', 'id', 'test_id');
    }
}
