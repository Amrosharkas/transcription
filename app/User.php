<?php

namespace App;
use App\Test;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Authenticatable implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     'dec_password','user_type', 'user_notes','status',  'name', 'email', 'password','city','mobile','test_taken','mail_sent','password_encrypted','pera_email','pera_password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserTest(){
        Return $this->hasOne('App\Test', 'user_id', 'id');
    }
    
}
