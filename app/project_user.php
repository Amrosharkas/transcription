<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project_user extends Model
{
    public function getProject(){
    	return $this->belongsTo('App\Project','project_id','id');
    }
    public function getUser(){
    	return $this->belongsTo('App\User','user_id','id');
    }

}
