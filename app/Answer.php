<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
	protected $table = 'answers';
    protected $fillable = ['user_id','test_id','q_id','correct','q_order','answer_type','answer_text'];
}
