<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Answer;
use Illuminate\Http\Request;
use Hash;
use Auth;
use App\Http\Requests;
use App\peras_user;
use App\Http\Controllers\Controller;
use App\project_user;
use App\pera_pera;
use Mail;
use DB;

class UserController extends Controller {
    public function redirect(){
        $user = Auth::user();
        
        
        if(isset($user)){
            if($user->user_type == "Prince"){
                return redirect('/admin/backend/allUsers?status=all');
            }
            if($user->user_type == "Transcriber"){
                return redirect('/admin/users');
            }

        }else{
            return redirect('/auth/login');

        }
    }

    public function testEmail() {
        $user = Auth::user();
        //return Hash::make($user->password);
        Mail::send('emails.welcome', ['user' => $user], function ($m) use ($user) {
            $m->from('transcription.project@arabiclocalizer.com', 'Arabic Localizer');

            $m->to('amrosharkas@hotmail.com', 'Amr Sharkas')->subject('New Account Information');
        });
    }

    public function createOriginalUser() {

        $data = array();
        $data['name'] = 'Mohamed Tarek';
        $data['email'] = 'mtarek@arabiclocalizer.com';
        $data['password'] = Hash::make('m.tarekPass123');
        $data['user_type'] = "Project Manager";
        //$user = User::create($data);

        $users = User::where('password_encrypted', 0)->get();

        foreach ($users as $user) {
            $user->password = Hash::make($user->dec_password);
            $user->password_encrypted = 1;
            $user->save();

            echo $user->name . " - ", $user->dec_password;
        }
    }

    

    public function sendWelcomeEmail($microsoft) {


        $matchthese = ['user_type' => 'Transcriber', 'mail_sent' => 0];
        if ($microsoft == 0) {
            $users = User::where($matchthese)->where('email', 'not like', '%hotmail%')->where('email', 'not like', '%outlook%')->where('email', 'not like', '%live%')->get();
        } else {
            $users = User::where($matchthese)->where('email', 'like', '%hotmail%')->orWhere($matchthese)->where('email', 'like', '%outlook%')->orWhere($matchthese)->where('email', 'like', '%live%')->get();
        }
        foreach ($users as $user) {

            Mail::send('emails.welcome', ['user' => $user], function ($m) use ($user) {
                $m->from('transcription.project@arabiclocalizer.com', 'Arabic Localizer');

                $m->to($user->email, $user->name)->subject('مرحبا في برنامج تدريب Arabic Localizer لمدخلي البيانات');
                $user->mail_sent = 1;
                $user->save();
                echo $user->email . " - sent</br>";
            });
        }
    }

    public function index() {
        $user = Auth::user();
        return view('admin.user_details', compact('user'));
    }

    public function userDetails($id) {

        $user = User::find($id);
        return view('admin.user_details', compact('user'));
    }

    public function addNote($id){
        $user = User::find($id);
        return view('admin.add_note', compact('user'));
    }

    public function save(Request $request) {
        $data = $request->input();
        $data['password'] = Hash::make($data['password']);
        if ($request->input('id') == "") {
            $user = User::create($data);
        } else {
            $user = User::find($request->input('id'))->update($data);
        }
        return 'success';
    }
    public function saveNote(Request $request) {
        $data = $request->input();
        
        
            $user = User::find($request->input('id'))->update($data);
        
        return redirect('/admin/backend/accepted');
    }

    
    

    

    public function pera_originate(){
        $pera_users = User::where('pera_email','!=','')->get();
        foreach($pera_users as $user){
            $pera_user = new peras_user();
            $pera_user->date_taken = date('Y-m-d H:i:s');
            $pera_user->user_id = $user->id;
            $pera_user->pera_email = $user->pera_email;
            $pera_user->save();

        }
    }

    
    

}
