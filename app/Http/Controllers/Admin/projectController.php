<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Project;
use App\project_user;
use App\log;
use App\log_detail;
use App\pera_pera;
use App\pera_log;
use App\peras_user;
use App\User;
use DB;
use Auth;

class projectController extends Controller
{
	// Managing abstract Pera Pera Accounts
    public function peraPeras(){
        
        return view('admin.pera_peras');
    }
    public function getPera_peras(){
        $model_name = 'pera_pera';
        $pera_peras = pera_pera::get();
        return view('admin.get_peras',compact('pera_peras','model_name'));
    }
    public function pera_edit($id){
            $user = User::find($id);
            
            return view('admin.pera_details',compact('user','pera_emails'));
    }
    public function peraDetails($id){
        $pera_pera = pera_pera::find($id);
        return view('admin.pera_details',compact('pera_pera'));
    }
    public function peraSave(Request $req){
        $data = $req->input();
        unset($data['_token']);
        if(isset($data['id'])){
            pera_pera::where('id',$data['id'])->update($data);
        }else{
            pera_pera::create($data);
        }

        
    }
    public function checkPeraEmail(Request $req){
        $email = pera_pera::where('pera_email',$_GET['pera_email'])->first();
         
        if(isset($email->pera_email)){
            return "false";
        }else{
            return "true";
        }

    }


    // Managing abstract Projects
    public function projects($status) {
        if($status == "active"){
            $projects = Project::where('project_active' , 1)->orderBy('project_active','desc')->get();
        }
        if($status == "completed"){
            $projects = Project::where('project_active' , 0)->orderBy('project_active','desc')->get();
        }
        return view('admin.backend.projects.index', compact('projects','status'));
    }
    
    public function addProject() {
        return view('admin.backend.projects.add');
    }

    public function saveProject(Request $request) {
        $project = new Project();
        $project->title = $request->title;
        $project->save();
        return redirect()->to('/admin/backend/projects');
    }

    public function project($id) {

        $project = Project::findOrFail($id);
        $logs = log::where('project_id',$id)->get();
        $projectUserIds = collect(DB::table('project_users')
                ->where('project_id', '=', $project->id)->get())
                ->pluck('user_id');
        
        $acceptedUsers = User::where('status', '=', 'accepted')
                        ->whereNotIn('users.id',$projectUserIds)
                        ->where('pera_email','!=','')
                        
                        ->orderBy('users.name', 'ASC')
                        ->get();
        echo view('admin.backend.projects.project', compact('project', 'acceptedUsers','logs'));
    }
    public function projectsChangeStatus($id){
        $project = Project::find($id);

        if($project->project_active == 1){
            $project->project_active = 0;
            
        }else{
        
            $project->project_active = 1;
        } 
         Project::where('id', $id)->update(['project_active' => $project->project_active]);
        
        return redirect('/admin/backend/projects/active');
    }

    
    // Project Users Management
    public function addUser(Request $request) {
        $user = User::findOrFail($request->user);
        DB::table('project_users')->insert([
            'user_id' => $user->id,
            'project_id' => $request->project
        ]);
        return redirect('/admin/backend/project/'.$request->project);

    }
    public function removeUser($user_id,$project_id){
        project_user::where('user_id',$user_id)->where('project_id',$project_id)->delete();
        return redirect('/admin/backend/project/'.$project_id);
    }
    
    

	// Project Logs Management
    public function addLog(Request $req){
        $data = $req->input();
        $admin_user = Auth::user();
        $log = new log();
        $log->datetime = date('Y-m-d H:i:s');
        $log->added_by = $admin_user->id;
        $log->project_id = $data['project_id'];
        $log->save();
        // Log::create([
        //     'datetime'=>date('Y-m-d H:i:s'),
        //     'added_by'=>$admin_user->id,
        //     'project_id'=>$data['project_id']
        //     ]);
        foreach($req->input() as $name => $value) {
            $splitString = explode("-", $name);
            
            // If the data begins with "log", use it.
            if ($splitString[0] == "log") {
                // Set the other desired values into variables.
                $user_id = $splitString[1];
                $user = User::find($user_id);
                $last_ammount = pera_log::where('pera_email',$user->pera_email)->where('project_id',$data['project_id'])->orderBy('id','desc')->first();
               
                
                $lam = 0;

                if($last_ammount){
                
                $lam = $last_ammount->ammount;
                 
                
                }
                $diff_ammount= $value - $lam;
                
                
                // Database query goes here.
                $log_detail = new log_detail();
                $log_detail->log_id =$log->id;
                $log_detail->project_id=$data['project_id'];
                $log_detail->added_by=$admin_user->id;
                $log_detail->user_id=$user_id;
                $log_detail->ammount = $value;
                $log_detail->diff_ammount = $diff_ammount;
                $log_detail->save();
                pera_log::create([
                    'pera_email' =>$user->pera_email,
                    'project_id'=>$data['project_id'],
                    'ammount' => $value,
                    'datetime'=>date('Y-m-d H:i:s'),
                    'log_detail_id' =>$log_detail->id
                ]);

            }

    
        }
        return redirect('/admin/backend/project/'.$data['project_id']);
    }
    public function updateLog(Request $req){
        $data = $req->input();
        
       $log = log::find($data['log_id']);
        foreach($req->input() as $name => $value) {
            $splitString = explode("-", $name);

            // If the data begins with "log", use it.
            if ($splitString[0] == "log") {
                // Set the other desired values into variables.
                $user_id = $splitString[1];
                $user = User::find($user_id);
                $log_detail =  log_detail::where('log_id',$log->id)->where('user_id',$user_id)->first();
                $last_ammount = pera_log::where('pera_email',$user->pera_email)->where('project_id',$data['project_id'])->where('log_detail_id', '<' ,$log_detail->id)->orderBy('id','desc')->first();
                $lam = 0;

                if($last_ammount){
                
                $lam = $last_ammount->ammount;
                 
                
                }
                $diff_ammount= $value - $lam;
                //Database query goes here.
                log_detail::where('log_id',$log->id)->where('user_id',$user_id)->update([
                  'ammount' => $value,
                  'diff_ammount' => $diff_ammount  
                        
                ]);
                pera_log::where('log_detail_id',$log_detail->id)->update([
                    
                    'ammount' => $value
                    
                ]);
                
                
                

            }
            
    
        }
        
        return redirect('/admin/backend/project/'.$data['project_id']);
    }
    public function checkLastValue($user_id,$project_id){
        $user = user::find($user_id);
        $last_value = pera_log::where('pera_email',$user->pera_email)->where('project_id',$project_id)->orderBy('id','desc')->first();
        if($last_value){
            return $last_value->ammount;
        }else{
            return 0;
        }
    }
    public function getLogDetails($id){
        $log = log::find($id);
        $check_last_log = log::where('project_id',$log->project_id)->where('id','>',$log->id)->count();
        $log_details = log_detail::where('log_id',$id)->get();
        $log_detail = log_detail::where('log_id',$id)->first();
        $project = project::where('id',$log_detail->project_id)->first();
        return view('admin.backend.projects.log_details',compact('log_details','project','log','check_last_log'));
    }

    
}
