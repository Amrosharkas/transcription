<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Question;
use App\Test_answer;
use App\Test;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class testController extends Controller {

    public function getTests() {


        return view('admin.getTests');
    }

    public function generateTest($user_id) {
        $check_test = Test::where('user_id', $user_id)->count();
        if ($check_test == 0) {
            $questions = Question::orderByRaw("RAND()")->get()->random(150);
            $test = new Test();
            $test->user_id = $user_id;
            date_default_timezone_set("Africa/Cairo");
            $test->start_time = date("Y-m-d H:i:s");
            $test->status = 2;
            $test->save();
            $order = 1;
            foreach ($questions as $question) {
                $answer = new Test_answer();
                $answer->user_id = $user_id;
                $answer->test_id = $test->id;
                $answer->q_id = $question->id;
                $answer->correct = 2;
                $answer->q_order = $order;
                $answer->save();
                $order++;
            }
            $this->checkTestStatus($test->id, 'ajax');
        } else {
            return 'Test already generated for this user';
        }
    }

    public function checkTestStatus($test_id, $from) {

        $user = Auth::user();
        $test = Test::where('id', $test_id)->count();
        if ($test == 0) {

            return view('admin.getTests');
            exit();
        }
        $test = Test::find($test_id);

        if ($test->status == 1) {

            return view('admin.test_finished');
            exit();
        }
        if ($test->status == 0) {

            return view('admin.getTests');
            exit();
        }
        if ($test->status == 2) {
            $match_these = ['correct' => 2, 'user_id' => $user->id];
            $get_next_question = Test_answer::where($match_these)->orderBy('q_order')->first();
            $get_next_question_count = Test_answer::where($match_these)->orderBy('q_order')->count();

            if ($get_next_question_count == 0) {

                $this->endTest($test_id);
            } else {
                $question = $get_next_question;
                $auido_file = 'Audio (' . $question->getOriginalQuestion->audio_number . ').WAV';
                if ($from != "form") {
                    echo view('admin.question', compact('question', 'auido_file'));
                } else {
                    echo view('admin.question_form', compact('question', 'auido_file'));
                }

                //echo 'Next question is '.$get_next_question->getOriginalQuestion->right_text1;
            }
        }
    }

    public function updateAnswer(Request $req) {
        $data = $req->input();


        $answer = Test_answer::find($data['id']);
        $data['correct'] = 0;
        
        
        if($data['answer_type'] == "Text"){
            
                if (trim($data['answer_text']) == trim($answer->getOriginalQuestion->right_text1)  || trim($data['answer_text']) ==  trim($answer->getOriginalQuestion->right_text2)) {
                    $data['correct'] = 1;
                }
            
        }else{
            if ($answer->getOriginalQuestion->right_type == $data['answer_type']) {
                
                $data['correct'] = 1;
                
            }
        }

        $id = $data['id'];
        unset($data['radio']);
        unset($data['_token']);
        unset($data['id']);

        //dump($data);
        $update = Test_answer::find($id)->update($data);
        $from = "form";
        $this->checkTestStatus($answer->test_id, $from);
    }

    public function goToQuestion($id) {
        $question = Test_answer::find($id);
        $auido_file = 'Audio (' . $question->getOriginalQuestion->audio_number . ').WAV';
        return view('admin.question_form', compact('question', 'auido_file'));
    }

    public function endTest($test_id) {
        $test = Test::find($test_id);
        date_default_timezone_set("Africa/Cairo");
        $test->end_time = date("Y-m-d H:i:s");
        $test->status = 1;
        $match_these = ['test_id' => $test_id, 'correct' => 1];
        $score = Test_answer::where($match_these)->count();
        $test->test_score = $score;
        $test->save();
        echo view('admin.test_finished');
    }

    public function dataExample(){
        $users = User::select("name",'email')->get();
        $users = $users->toArray();
        
        $count = User::select("name",'email')->count();
        $data = array();
        $data['draw'] = 1;
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count;
        $dataUsers = array();
        foreach($users as $key=> $value){
            $value = str_replace('', '"name":"', $value);
            $value = str_replace('', '"email":"', $value);
            array_push($dataUsers,$value);
        }
        $data['data'] = $dataUsers;
        return json_encode($data);
        
    }
}
