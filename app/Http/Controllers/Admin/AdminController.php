<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\User_holiday;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller {

    public function delete($model, $id) {

        $model_name = 'App\\' . $model;
        $user = new $model_name();
        $user::find($id)->delete();
        
    }

    public function deleteMultiple($model, Request $request) {
        $data = $request->input();
        $selected_users_id = count($data["users"]);
        $model_name = 'App\\' . $model;
        $user = new $model_name();
        $return = "";
        for ($i = 0; $i < $selected_users_id; $i++) {

            $user_id = trim(strip_tags($_POST["users"][$i]));
            $return .= $user_id . ",";
            $user::find($user_id)->delete();
            
        }

        return $return;
    }

}
