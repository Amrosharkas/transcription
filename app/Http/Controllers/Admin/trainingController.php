<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Answer;

class trainingController extends Controller
{
    public function getSessions() {

        $model_name = "User";
        return view('admin.get_sessions', compact('model_name'));
    }

    public function viewSession($id) {

        $model_name = "User";
        return view('admin.view_session', compact('model_name', 'id'));
    }

    public function getExamples() {
        $answers = Answer::get();
        $model_name = "User";
        return view('admin.get_examples', compact('model_name', 'answers'));
    }
}
