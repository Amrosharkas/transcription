<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Test;
use DB;
use App\Test_answer;
use App\payment;
use Auth;
use App\pera_pera;
use App\peras_user;
use Datatables;
use Hash;
use Mail;

class BackendController extends Controller {
    public function originate(){
        DB::select('update pera_peras set email_taken = 1 WHERE pera_email in (select pera_email from peras_users where date_left is not null');

    }

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $model_name = 'User';
        return view('allUsers',compact('model_name'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUsers($status,Request $request)
    {
        if ($status == 'new') {
        $users = DB::table('users')
                
                ->where('users.status', '=', 'new')
            
                ->leftJoin('tests', 'users.id', '=', 'tests.user_id')
                ->select(  'name', 'test_score','start_time','end_time',DB::raw('TIMEDIFF(tests.end_time,tests.start_time) AS duration'),'users.id as user_id');
        }else{
        $users = DB::table('users')
  
                ->leftJoin('tests', 'users.id', '=', 'tests.user_id')
                ->select(  'name', 'test_score','start_time','end_time',DB::raw('TIMEDIFF(tests.end_time,tests.start_time) AS duration'),'users.id as user_id');  
        }
        //$users = User::select(['name','email']);

        return Datatables::of($users)
        ->addColumn('Test', function ($user) {
                return '<a href="/admin/backend/'.$user->user_id.'/results" target="_blank" ><button type="button" class="btn green-seagreen">Test</button></a>';
            })
        ->addColumn('Checked', function ($user) {
                return '<input class="update-checked" data-user="'.$user->user_id.'" type="checkbox">';
            })
        ->addColumn('Checkbox', function ($user) {
                return '<input name="users[]" class="checkboxes" value="'.$user->user_id.'" type="checkbox">';
            })
        ->filter(function ($query) use ($request) {
            if ($request->has('name')) {
                $query->where('name', 'like', "%{$request->get('name')}%");
            }

            if ($request->has('email')) {
                $query->where('email', 'like', "%{$request->get('email')}%");
            }

            if ($request->has('date_start')) {
                $query->where('start_time', '>', "{$request->get('date_start')}");
            }
            if ($request->has('date_end')) {
                $query->Where('end_time', '<', "{$request->get('date_end')}");
            }
            if ($request->has('date_from')) {
                $query->where('users.created_at', '>', "{$request->get('date_from')}");
            }
            if ($request->has('date_to')) {
                $query->Where('users.created_at', '<', "{$request->get('date_to')}");
            }
            if ($request->has('score_from')) {
                $query->Where('test_score', '>=', "{$request->get('score_from')}");
            }
            if ($request->has('score_to')) {
                $query->Where('test_score', '<=', "{$request->get('score_to')}");
            }
            if ($request->has('checked') && $request->get('checked')!=2 ) {
                $query->Where('tests.checked', '=', "{$request->get('checked')}");
            }
            
            

            
        })
        ->setRowID(function ($user) {
                return "data-row-".$user->user_id ;
        })
        ->make(true);
        
            
    }

    public function index($filter = null) {
        if ($filter == null) {
            $filterRoute = 'getUsers';
        } elseif ($filter == 'accepted') {
            $filterRoute = 'getAcceptedUsers';
        }elseif ($filter == 'all') {
            $filterRoute = 'getAllUsers';
        } elseif ($filter == 'rejected') {
            $filterRoute = 'getRejectedUsers';
        }elseif ($filter == 'dismissed') {
            $filterRoute = 'getDismissedUsers';
        } else {
            abort(404);
        }
        return view('admin.backend.index', compact('filterRoute'));
    }

    public function getUsers() {
        $query = DB::table('users')
                ->where('users.status', '=', 'new')
                ->leftJoin('tests', 'users.id', '=', 'tests.user_id')
                ->select('users.*', 'tests.start_time', 'tests.end_time', 'tests.test_score', 'tests.checked', DB::raw('TIMEDIFF(tests.end_time,tests.start_time) AS duration'));
        $users = collect($query->get());
        $stats['total'] = $query->count();
        $stats['total_started'] = $query->whereNotNull('tests.start_time')->count();
        $stats['total_ended'] = $query->whereNotNull('tests.end_time')->count();
        $model_name = "User";
        return view('admin.get_users', compact('users', 'model_name', 'stats'));
    }
    

    public function getAcceptedUsers() {
        $query = DB::table('users')
                ->where('users.status', '=', 'accepted')
                ->leftJoin('tests', 'users.id', '=', 'tests.user_id')
                ->select('users.*', 'tests.test_score');
        $users = collect($query->get());
        echo view('admin.accepted_users', compact('users'));
    }
    public function dismissUser($user_id){
        $user = User::find($user_id)->update(['status'=>'dismissed']);
        return $this->getAcceptedUsers();
    }
    public function getDismissedUsers() {
        $query = DB::table('users')
                ->where('users.status', '=', 'dismissed')
                ->leftJoin('tests', 'users.id', '=', 'tests.user_id')
                ->select('users.*', 'tests.start_time', 'tests.end_time', 'tests.test_score', 'tests.checked', DB::raw('TIMEDIFF(tests.end_time,tests.start_time) AS duration'));
        $users = collect($query->get());
        $stats['total'] = $query->count();
        $stats['total_started'] = $query->whereNotNull('tests.start_time')->count();
        $stats['total_ended'] = $query->whereNotNull('tests.end_time')->count();
        $model_name = "User";
        return view('admin.get_dismissed', compact('users', 'model_name', 'stats'));
    }

    public function getRejectedUsers() {
        $query = DB::table('users')
                ->where('users.status', '=', 'rejected')
                ->leftJoin('tests', 'users.id', '=', 'tests.user_id')
                ->select('users.*', 'tests.start_time', 'tests.end_time', 'tests.test_score', 'tests.checked');
        $users = collect($query->get());
        return view('admin.rejected_users', compact('users'));
    }

    public function acceptMultiple(Request $request) {
        DB::table('users')->whereIn('id', $request->users)->update(['status' => 'accepted']);
        return response()->json($request->users);
    }

    public function rejectMultiple(Request $request) {
        DB::table('users')->whereIn('id', $request->users)->update(['status' => 'rejected']);
        return response()->json($request->users);
    }

    public function results($id) {
        $user = User::findOrFail($id);
        $user_name = $user->name;
        $test = Test::where('user_id', '=', $id)->first();
        if (!$test || !$test->end_time) {
            abort(404);
        }
        $answers = collect(
                DB::table('test_asnwers')
                        ->where('test_id', '=', $test->id)
                        ->join('questions', 'test_asnwers.q_id', '=', 'questions.id')
                        ->select('test_asnwers.*', 'questions.audio_number', 'questions.right_type', 'questions.right_text1', 'questions.right_text2')
                        ->orderBy('correct')
                        ->get()
        );
        return view('admin.backend.results', compact('user_name', 'answers'));
    }

    public function details($id) {
        $user = User::find($id);
        return view('admin.backend.details', compact('user'));
    }

    public function updateCorrect(Request $request) {
        $answer = Test_answer::find($request->id);
        $test_id = $answer->test_id;
        $answer->correct = $request->val;
        $answer->save();
        $test = Test::where('test_id', '=', $test_id)->first();
        $test->test_score = Test_answer::where(['user_id' => $user_id, 'correct' => 1])->count();
        $test->save();
    }

    public function updateChecked(Request $request) {
        $test = Test::where('user_id', '=', $request->user_id)->first();
        $test->checked = $request->checked;
        $test->save();
    }

    

    public function getUserPayments($user_id) {
        $user = User::find($user_id);
        $payments = payment::where('user_id',$user_id)->get();
        $balance = $this->calculateBalance($user_id);
        $utts =  DB::select('SELECT aa.* ,(select title from projects pp where pp.id = aa.project_id) project, ( select created_at from log_details bb where aa.id > bb.id and aa.project_id = bb.project_id and aa.user_id = bb.user_id order by id desc limit 1   ) date_from , aa.diff_ammount*0.1 as payment  FROM log_details aa WHERE user_id = '.$user_id.' having aa.diff_ammount > 0');
       
        $uttPayment = 0;
        return view('admin.payments', compact('payments','balance','utts','user'));
    }
    public function calculateBalance($user_id){
        //$getTotalUtt = log_detail::where('user_id',$user_id)->sum('diff_ammount')->groupBy('project_id');
        $getProjectUtts =  DB::table('log_details')
                        ->where('user_id',$user_id)
                        ->select( DB::raw('SUM(diff_ammount) as totalUtt'))
                        ->groupBy('project_id')
                        ->get();
        $uttPayment = 0;
        foreach($getProjectUtts as $ProjectUtt){
            $uttPayment += $ProjectUtt->totalUtt * 0.1;
        }
        $payments_done = payment::where('user_id',$user_id)->sum('payment');

        $balance= $uttPayment - $payments_done;

        return $balance;
    }

    // Assigning and detaching Pera Pera accounts to users
    public function assignForm($id){
            $user = User::find($id);
            $pera_emails = pera_pera::where('email_taken',0)->get();
            return view('admin.pera_assign',compact('user','pera_emails'));
    }
    

    
    public function assignPera(Request $req){
        $data = $req->input();
        $pera_account = pera_pera::where('pera_email',$data['pera_email'])->first();
        $pera_user = new peras_user();

        $pera_user->pera_email = $data['pera_email'];
        $pera_user->user_id = $data['user_id'];
        $pera_user->date_taken = date("Y-m-d H:i:s");
        $pera_user->pera_status = 1;
        $pera_user->save();

        $user = User::find($data['user_id'])->update(['pera_email' =>  $data['pera_email']]);

        $pera_account->email_taken = 1;
        $pera_account->save();
        return redirect('/admin/backend/accepted');
    }
    public function pera_detach($user_id){
        $user = User::find($user_id);
        $pera_account = pera_pera::where('pera_email',$user->pera_email)->first();
        $pera_account->email_taken =  0;
        $pera_account->save();
        $pera_user = peras_user::where('pera_email',$user->pera_email)->where('user_id',$user->id)->orderBy('date_taken','desc')->first();
        if($pera_user){
            $pera_user->date_left = date('Y-m-d H:i:s');
            $pera_user->pera_status = 0;
            $pera_user->save();
        }

        
        $user->pera_email = '';
        $user->save();

    }


    public function detachUser(Request $req){
        $data = $req->input();
        $admin_user = Auth::user();
        foreach($req->input() as $name => $value) {
            $splitString = explode("-", $name);
            
            // If the data begins with "log", use it.
            if ($splitString[0] == "log") {
                // Set the other desired values into variables.
                $project_id = $splitString[1];
                $project = User::find($project_id);
                
                $log = new log();
                $log->datetime = date('Y-m-d H:i:s');
                $log->added_by = $admin_user->id;
                $log->project_id = $project_id;
                $log->save();
                $users = project_user::where('project_id',$project_id)->get();

                        
                foreach($users as $user){
                    $last_ammount = log_detail::where('project_id',$project_id)->where('user_id',$user->user_id)->orderBy('id','desc')->first();
                    $lam = 0;
                    $dam = 0;
                    if($last_ammount){
                        $lam = $last_ammount->ammount;
                        $dam = $last_ammount->diff_ammount;
                    }
                    if($user->user_id == $data['user_id']){
                        $nam = $data['log-'.$project_id];
                        $ndam = $nam - $lam;
                    }else{
                        $nam = $lam;
                        $ndam = $dam;
                    }
                    // Database query goes here.
                    $log_detail = new log_detail();
                    $log_detail->log_id =$log->id;
                    $log_detail->project_id=$project_id;
                    $log_detail->added_by=$admin_user->id;
                    $log_detail->user_id=$user->user_id;
                    $log_detail->ammount = $nam;
                    $log_detail->diff_ammount = $ndam;
                    $log_detail->save();
                    pera_log::create([
                        'pera_email' =>$user->getUser->pera_email,
                        'project_id'=>$project_id,
                        'ammount' => $nam,
                        'datetime'=>date('Y-m-d H:i:s'),
                        'log_detail_id' =>$log_detail->id
                    ]);
                    if($user->user_id == $data['user_id']){
                       $this->pera_detach($user->user_id);
                       project_user::where('user_id',$user->user_id)->where('project_id',$project_id)->delete();
                   }
                }


            }

    
        }


    }
    public function pera_finalize_detach($user_id){
        $user = User::find($user_id);
        $projects = DB::select('SELECT pu.project_id ,pr.title , IFNULL(( select ammount from log_details ld where ld.user_id = pu.user_id and ld.project_id = pu.project_id order by id desc limit 1 ),0) last_value FROM `project_users` pu , projects pr WHERE pu.user_id = '.$user_id.' and pu.project_id = pr.id order by pu.project_id asc');
        if($projects){

        return view('admin.finalize_detach',compact('user','projects'));
        }else{
            $this->pera_detach($user_id);
            $this->getAcceptedUsers();
        }


    }
    public function addUser(){
        
        return view('admin.add_user');
    }

    public function formAddUser(){
        $user = null;
        return view('admin.form_add_user',compact('user'));
    }
    public function doAddUser(Request $request){
        $data = $request->input();
        $data['dec_password'] = $this->randomPassword();
        $data['password'] = Hash::make($data['dec_password']);
        $data['user_type'] = "Transcriber";
        $user = User::create($data);

        Mail::send('emails.welcome', ['user' => $user], function ($m) use ($user) {
            $m->from('transcription.project@arabiclocalizer.com', 'Arabic Localizer');

            $m->to($user->email, $user->name)->subject('مرحبا في برنامج تدريب Arabic Localizer لمدخلي البيانات');
            $user->mail_sent = 1;
            $user->save();
        });

        return 'success';
    }
    public function checkEmailAddUser(){
        $email = User::where('email',$_GET['email'])->first();
        if(isset($email->email)){
            return "false";
        }else{
            return "true";
        }
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 4; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $pass = implode($pass); //turn the array into a string
        return "arabloc-".$pass;
    }
    


}
