<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Datatables;
use App\User;
use App\Test;
use DB;

class dataTablesController extends Controller
{
    /**
	 * Displays datatables front end view
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex()
	{
		$model_name = 'User';
	    return view('allUsers',compact('model_name'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getAllUsers(Request $request)
	{
		$users = DB::table('users')
                ->where('users.status', '=', 'new')
                ->leftJoin('tests', 'users.id', '=', 'tests.user_id')
                ->select(  'name', 'test_score','start_time','end_time',DB::raw('TIMEDIFF(tests.end_time,tests.start_time) AS duration'),'users.id as user_id');
       
	    //$users = User::select(['name','email']);

        return Datatables::of($users)
        ->addColumn('Test', function ($user) {
                return '<a href="/admin/backend/'.$user->user_id.'/results" target="_blank" ><button type="button" class="btn green-seagreen">Test</button></a>';
            })
        ->addColumn('Checked', function ($user) {
                return '<input class="update-checked" data-user="'.$user->user_id.'" type="checkbox">';
            })
        ->addColumn('Checkbox', function ($user) {
                return '<input name="users[]" class="checkboxes" value="'.$user->user_id.'" type="checkbox">';
            })
        ->filter(function ($query) use ($request) {
            if ($request->has('name')) {
                $query->where('name', 'like', "%{$request->get('name')}%");
            }

            if ($request->has('email')) {
                $query->where('email', 'like', "%{$request->get('email')}%");
            }

            if ($request->has('date_start')) {
                $query->where('start_time', '>', "{$request->get('date_start')}");
            }
            if ($request->has('date_end')) {
                $query->Where('end_time', '<', "{$request->get('date_end')}");
            }
            if ($request->has('date_from')) {
                $query->where('users.created_at', '>', "{$request->get('date_from')}");
            }
            if ($request->has('date_to')) {
                $query->Where('users.created_at', '<', "{$request->get('date_to')}");
            }
            if ($request->has('score_from')) {
                $query->Where('test_score', '>=', "{$request->get('score_from')}");
            }
            if ($request->has('score_to')) {
                $query->Where('test_score', '<=', "{$request->get('score_to')}");
            }
            if ($request->has('checked') && $request->get('checked')!=2 ) {
                $query->Where('tests.checked', '=', "{$request->get('checked')}");
            }

            
        })
        ->make(true);
        
            
	}


}
