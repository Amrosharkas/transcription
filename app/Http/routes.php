<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */
Route::group(['prefix' => 'admin/backend', 'namespace' => 'Admin', 'middleware' => ['web', 'auth', 'backend']], function () {

// Users management --------------------------------
    
    // All Users Datatables
    Route::controller('allUsers', 'BackendController', [
        'getAllUsers'  => 'datatables.data',
        'getIndex' => 'datatables',
    ]);
    Route::get('multi-filter-select-data/{status}', 'BackendController@getAllUsers');
    
    Route::get('getAcceptedUsers', 'BackendController@getAcceptedUsers');
    Route::get('getRejectedUsers', 'BackendController@getRejectedUsers');
    Route::get('getDismissedUsers', 'BackendController@getDismissedUsers');
    Route::get('addUser', 'BackendController@addUser');
    Route::get('formAddUser', 'BackendController@formAddUser');
    Route::post('doAddUser', 'BackendController@doAddUser');
    Route::get('checkEmailAddUser', 'BackendController@checkEmailAddUser');
    Route::post('update-correct', 'BackendController@updateCorrect');
    Route::post('update-checked', 'BackendController@updateChecked');
    Route::post('reject-multiple', 'BackendController@rejectMultiple');
    Route::post('accept-multiple', 'BackendController@acceptMultiple');
    Route::get('user/add_note/{id}', 'UserController@addNote');
    Route::get('payments/{user_id}', 'BackendController@getUserPayments');
    Route::get('payments/{user_id}', 'BackendController@getUserPayments');
    Route::get('{id}/details', 'BackendController@details');
    Route::get('calcBalance/{id}', 'BackendController@calculateBalance');
    Route::get('{id}/results', 'BackendController@results');
    
   


    // Project Management ------------------------------------------------
    Route::get('projects/{status}', 'projectController@projects');
    Route::get('project/{id}', 'projectController@project');
    Route::get('projects/add', 'projectController@addProject');
    Route::post('projects/save', 'projectController@saveProject');
    Route::get('project/change_status/{id}', 'projectController@projectsChangeStatus');
    Route::post('project/add-user','projectController@addUser');
    Route::get('remove/{user_id}/{project_id}', 'projectController@removeUser');
    Route::post('log/submit', 'projectController@addLog');
    Route::post('log/update', 'projectController@updateLog');
    Route::get('log/{id}', 'projectController@getLogDetails');
    Route::get('project/checkLastValue/{user_id}/{project_id}',  'projectController@checkLastValue');
    

    // Pera Pera Assigning ------------------------------------------------
    Route::get('pera/assign/{user_id}', 'BackendController@assignForm');
    Route::get('pera/{id}', 'BackendController@pera_edit');
    Route::get('pera/{user_id}/detach',  'BackendController@pera_finalize_detach');
    Route::get('pera/originate/or', 'BackendController@pera_originate');
    
   
    // Pera Pera Emails management ----------------------------
    Route::get('pera_peras', 'projectController@peraPeras');
    Route::get('getPera_peras', 'projectController@getPera_peras');
    Route::get('pera_peras/add/{id}', 'projectController@peraDetails');
    Route::get('pera_pera/{id}', 'projectController@peraDetails');
    Route::get('checkEmail', 'projectController@checkPeraEmail');
    Route::post('pera_peras/save', 'projectController@peraSave');




    Route::get('{filter?}', 'BackendController@index');
    
    
    
});

Route::group(['middleware' => ['web' , 'auth']], function () {
    //  Route::get('admin/originate', [
    //      'uses' => 'Admin\UserController@createOriginalUser'
    //  ]);
    // Route::get('admin/testEmail', [
    //      'uses' => 'Admin\UserController@testEmail'
    //  ]);
    //  Route::get('admin/sendWelcomeEmail/{microsoft}', [
    //      'uses' => 'Admin\UserController@sendWelcomeEmail'
    //  ]);
    // Admin Routes
    

       
        // Users ------------------------------
        Route::get('admin/users', function () {
            return view('admin.users');
        });
        Route::get('admin/getUsers', [
            'uses' => 'Admin\UserController@index'
        ]);

        Route::get('admin/user/add', function () {
            return view('admin.user_details');
        });
        Route::get('admin/payments', function () {
            return view('admin.master_payments');
        });
        Route::post('/admin/user/save', [
            'uses' => 'Admin\UserController@save'
        ]);
        Route::post('/admin/user/saveNote', [
            'uses' => 'Admin\UserController@saveNote'
        ]);
        Route::post('/admin/user/pera_save', [
            'uses' => 'Admin\UserController@pera_save'
        ]);
        Route::get('/admin/payments/{user_id}', [
            'uses' => 'Admin\BackendController@getUserPayments'
        ]);
        Route::get('/admin/dismiss/{user_id}', [
            'uses' => 'Admin\BackendController@dismissUser'
        ]);
        Route::get('/admin/getDismissedUsers', [
            'uses' => 'Admin\BackendController@getDismissedUsers'
        ]);
        Route::post('/admin/detach', [
            'uses' => 'Admin\BackendController@detachUser'
        ]);
         Route::post('/admin/assign_pera', [
            'uses' => 'Admin\BackendController@assignPera'
        ]);
        

        
        
        // Training ------------------------------
        Route::get('admin/training', function () {
            return view('admin.training');
        });
        Route::get('admin/getSessions', [
            'uses' => 'Admin\trainingController@getSessions'
        ]);

        Route::get('admin/sessions/{id}', [
            'uses' => 'Admin\trainingController@viewSession'
        ]);
        Route::get('admin/getExamples', [
            'uses' => 'Admin\trainingController@getExamples'
        ]);

        // Tests ------------------------------
        Route::get('admin/test', function () {
            return view('admin.test');
        });
        Route::get('admin/getTests', [
            'uses' => 'Admin\testController@getTests'
        ]);
        Route::get('admin/generateTest/{id}', [
            'uses' => 'Admin\testController@generateTest'
        ]);
        Route::post('/admin/updateAnswer', [
            'uses' => 'Admin\testController@updateAnswer'
        ]);
        Route::get('admin/checkTest/{id}/{from}', [
            'uses' => 'Admin\testController@checkTestStatus'
        ]);
        Route::get('admin/endTest/{test_id}', [
            'uses' => 'Admin\testController@endTest'
        ]);

        // Deleting  ------------------------------
        Route::get('/admin/delete/{model}/{id}', [
            'uses' => 'Admin\AdminController@delete'
        ]);
        Route::post('/admin/delete/multiple/{model}', [
            'uses' => 'Admin\AdminController@deleteMultiple'
        ]);
        
    });
   

    

    

Route::group(['middleware' => ['web']], function () {
// Authentication routes...
    Route::get('/auth/login', 'Auth\AuthController@getLogin');
    Route::post('/auth/login', 'Auth\AuthController@postLogin');
    Route::get('/auth/logout', 'Auth\AuthController@logout');
    Route::get('/', 'Admin\UserController@redirect');

    
   
   

 });

