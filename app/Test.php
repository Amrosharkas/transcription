<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model {

    protected $fillable = ['user_id', 'start_time', 'status', 'correct', 'q_order', 'answer_type', 'answer_text'];
    protected $dates = ['start_time', 'end_time', 'created_at', 'updated_at'];

}
