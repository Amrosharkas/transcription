<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log extends Model {

    protected $fillable = ['datetime', 'added_by', 'project_id'];
    protected $dates = ['datetime', 'created_at', 'updated_at'];

}
