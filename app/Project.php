<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Log;
class Project extends Model
{
	
	protected $fillable = [
        'title', 'project_active'
    ];
    public function getProjectUsers(){
    	return $this->belongsToMany('App\User', 'project_users', 'project_id', 'user_id');
    }
    public function getProjectLog(){
    	return $this->hasMany('App\Log', 'project_id', 'projects.id');
    }
}
