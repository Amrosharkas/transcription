<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log_detail extends Model
{

    protected $fillable = ['added_by','project_id','user_id','log_id','ammount'];

    public function getUser(){
    	Return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getProject(){
    	Return $this->hasOne('App\Project', 'id', 'project_id');
    }

    public function getLog(){
    	Return $this->hasOne('App\log', 'id', 'log_id');
    }

}
